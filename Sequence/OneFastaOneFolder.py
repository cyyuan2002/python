#!/usr/bin/env python
import sys
import os
from Bio import SeqIO

def output_fasta(fn, field=None):
    fh = open(fn, "rU")
    absfolder = os.path.abspath(fn)
    seq_folder = os.path.dirname(absfolder)
    for record in SeqIO.parse(fh, "fasta"):
        seq_ID = record.id
        if field is None:
            seqname = seq_ID
            seqname.replace("|", "_")
        else:
            seqnames = seq_ID.split("|")
            seqname = seqnames[field]

        outfolder = seq_folder + os.sep + seqname
        outfile = outfolder + os.sep + seqname + ".fa"
        if not os.path.isfile(outfolder):
            os.mkdir(outfolder)
        fout = open(outfile, "a")
        fout.write(record.format("fasta"))
        fout.close()

if __name__ == "__main__":

    if len(sys.argv) < 2:
        print("Usage:%s <fasta_file> [field_num]" %(sys.argv[0]))
        sys.exit(1)

    if len(sys.argv) == 2:
        output_fasta(sys.argv[1])
    else:
        output_fasta(sys.argv[1], int(sys.argv[2]))

    sys.exit(0)

