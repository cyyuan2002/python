#!/usr/bin/env python

import csv
import sys
import argparse
import numpy as np
from os.path import isfile
from Bio.Alphabet import IUPAC, Gapped
from Bio import AlignIO
from Bio.Align import MultipleSeqAlignment as MultiAlign
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq

def readAlleleType(AltypeFile):
    fhfile = open(AltypeFile, 'rU')
    reader = csv.reader(fhfile, delimiter='\t')
    alleleSeq = {}
    alleleIso = {}
    for line in reader:
        if(line[1] not in alleleSeq):
            alleleSeq[line[1]] = line[2]
            tmpisonames = []
            tmpisonames.append(line[0])
            alleleIso[line[1]] = tmpisonames
        else:
            tmpisonames = alleleIso[line[1]]
            tmpisonames.append(line[0])
            alleleIso[line[1]] = tmpisonames
    return (alleleSeq, alleleIso)

def readGroupInfo(GroupFile):
    fhfile = open(GroupFile, 'rU')
    reader = csv.reader(fhfile, delimiter='\t')
    isogroup = {}
    for line in reader:
        isogroup[line[0]] = line[1]
    
    return (isogroup)

def buildNexusInfo(AlleleSeq,AlleleIso,IsoGroup):
    SeqAlignment = MultiAlign(None,alphabet=Gapped(IUPAC.ambiguous_dna))
    SortedSeqs = sorted(AlleleSeq, key = int)
    for seqId in SortedSeqs:
        seqrecid = "seq" + seqId
        seq = Seq(AlleleSeq[seqId],Gapped(IUPAC.ambiguous_dna))
        seqrec = SeqRecord(seq,id = seqrecid)
        SeqAlignment.append(seqrec)
    
    groupset = sorted(set(IsoGroup.values()), key = str)
    
    zeros = [0] * len(groupset)
    AlleleNum = {}
    for iso in AlleleIso:
        isogroup = AlleleIso[iso]
        groupnum = zeros[:]
        for isoname in isogroup:
            groupidx = groupset.index(IsoGroup[isoname])
            groupnum[groupidx] += 1
        seqrecid = "seq" + iso
        AlleleNum[seqrecid] = groupnum
    
    return (SeqAlignment, AlleleNum, groupset)
    
def nexusOutput(SeqAlignment, AlleleNum, GroupSet, Outfile):
    fh_outfile = open(outfile,'w')
    AlignIO.write(SeqAlignment,fh_outfile,'nexus')
    fh_outfile.write("\nBEGIN TRAITS;\n\tDimensions NTRAITS=%s;\n\tFormat labels=yes missing=? separator=Comma;\n" %(len(GroupSet)))
    fh_outfile.write("\tTraitLabels %s;\n\tMatrix\n" %(" ".join(GroupSet)))
    for seqid in AlleleNum:
        groupnum = AlleleNum[seqid]
        fh_outfile.write("%s %s\n" %(seqid, ','.join(map(str, groupnum))))
    fh_outfile.write(";\n\nEND;\n")
    fh_outfile.close()

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Convert allele type file into nexus file:')
    parser.add_argument('-i', dest='input_file', help='allele type file')
    parser.add_argument('-g', dest='group_file', help='group file')
    parser.add_argument('-o', dest='output_file', help='output file')
    
    args = parser.parse_args()
    
    if(len(sys.argv)<3):
        parser.print_help()
        sys.exit(1)
    
    infile = args.input_file
    groupfile = args.group_file
    outfile = args.output_file
    
    AlleleSeq, AlleleIso = readAlleleType(infile)
    IsoGroup = readGroupInfo(groupfile)
    SeqAlignment, AlleleNum, GroupSet = buildNexusInfo(AlleleSeq,AlleleIso,IsoGroup)
    nexusOutput(SeqAlignment, AlleleNum, GroupSet, outfile)
    