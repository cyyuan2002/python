#!/usr/bin/env python
import sys
import csv
from Bio import SeqIO
from Bio import SeqRecord
from Bio.Seq import Seq

if(len(sys.argv) < 3):
    print(("Usage:%s <FastaFile> <PopInfo> <PopName, default:'POP'>" %(sys.argv[0])))
    sys.exit(1)

FasFile = sys.argv[1]
PopFile = sys.argv[2]
GroupName = 'POP'
if(sys.argv == 4):
    GroupName = sys.argv[3]

fh_popfile = open(PopFile,'r')
popinfo = {}
reader = csv.reader(fh_popfile, delimiter = "\t")
for line in reader:
    popinfo[line[0]] = line[1]
fh_popfile.close()


fh_fasfile = open(FasFile,'r')
outfile = FasFile + ".pop"
fh_outfile = open(outfile,'w')
popcount = {}
newID = {}
for seq in SeqIO.parse(fh_fasfile,"fasta"):
    if(seq.id not in popinfo):
        print(("Warning:can't find sequence id %s in population information!" %(seq.id)))
        continue
    if(popinfo[seq.id] in popcount):
        popcount[popinfo[seq.id]] += 1
    else:
        popcount[popinfo[seq.id]] = 1
    seqID = GroupName + popinfo[seq.id] + "_" + str(popcount[popinfo[seq.id]])
    newID[seq.id] = seqID
    seq.id = seqID
    fh_outfile.write(seq.format("fasta"))
fh_fasfile.close()

outid = FasFile + ".ids"
fh_outidfile = open(outid,'w')
for seqid in newID:
    fh_outidfile.write(seqid + "\t" + newID[seqid] + "\n")
fh_outidfile.close()
exit(0)
