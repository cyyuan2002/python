#!/usr/bin/env python
import sys
import csv
import re
from Bio import SeqIO

SeqFile = sys.argv[1]
SeqName = sys.argv[2]

SeqNames = {}

if len(sys.argv) < 2:
    print(("Usage: %s <Seq_File> <Seq_Names>" %(sys.argv[0])))

fh_seqname = open(SeqName, 'r')
csv_seqname = csv.reader(fh_seqname, delimiter = '\t')
for line in csv_seqname:
    SeqNames[line[0]] = line[1]

fh_seqname.close()

fh_fasta = open(SeqFile, 'rU')
for seq in SeqIO.parse(fh_fasta,"fasta"):
    if seq.id in list(SeqNames.keys()):
        seq.id = SeqNames[seq.id]
    seq.description = ""
    print((seq.format('fasta')))

fh_fasta.close()
