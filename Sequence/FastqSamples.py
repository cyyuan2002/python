# this script is used to get random samples from fastq file to fasta file

from Bio import SeqIO
import sys
import random

if len(sys.argv) < 4:
    print("Usage:%s <Fastq_File> <Output_File> <Sample_Size> [Filter_File]")
    sys.exit(0)

seq_file = sys.argv[1]
out_file = sys.argv[2]
sample_size = int(sys.argv[3])
filter_file = None

if len(sys.argv) == 5:
    filter_file = sys.argv[4]

filter_list = list()
if filter_file is not None:
    filter_list = open(filter_file).read().splitlines()

handle = open(seq_file, 'rU')
seqs = list()

for seq in SeqIO.parse(handle, "fastq-sanger"):
    if seq.id not in filter_list:
        seqs.append(seq)

samples_indices = random.sample(range(len(seqs)), sample_size)

with open(out_file, 'w') as fh_out:
    for index in samples_indices:
        fh_out.write(seqs[samples_indices].format("fasta"))

