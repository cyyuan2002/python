#!/usr/bin/env python
import sys
import argparse
import os.path

def check_dir(dr):
    if os.path.exists(dr):
        if not os.path.isdir(dr):
            sys.stderr.write("Error: target folder is a file.\n")
            sys.exit(1)
        else:
            return True
    else:
        os.mkdir(dr)
        return True

def sp_reads(fqa, fqb, mode, N, fout, outDir):
    """ Split fastq file based on the mode"""
    
    #total number of lines of the file
    numofline = sum(1 for _ in open(fqa))
    records =  numofline / 4
    
    #N is used as number of reads and files based on mode
    if mode == 0:
        readperFile = N
        if (records % N == 0):
            numofFiles = records/N
        else:
            numofFiles = records/N + 1
    elif mode == 1:
        numofFiles = N
        if (records % N == 0):
            readperFile = records/N
        else:
            readperFile = records/N + 1
    
    sys.stderr.write("Number of reads: %s\n" %(records))
    sys.stderr.write("Number of files: %s\n" %(numofFiles))
    sys.stderr.write("Number of reads in each file: %s\n\n" %(readperFile))
    
    
    fha = open(fqa)
    if fqb:
        fhb = open(fqb)
    
    fileIndex = 1
    
    check_dir(outDir)
        
    if fqb:
        fout1 = outDir + '/' + fout + "_1_%s.fastq" %(fileIndex)
        suba = open(fout1, "w")
        fout2 = outDir + '/' + fout + "_2_%s.fastq" %(fileIndex)
        suba = open(fout2, "w")
    else:
        fout1 = outDir + '/' + fout + "_%s.fastq" %(fileIndex)
        suba = open(fout1, "w")
    
    sys.stderr.write("Writing sequences to file %s...\n" %(fileIndex))
    
    #Total line count
    linecount = 0
    #Read count for each file, reset when create new file, accumulate when rec_no equals to 4
    recount = 0
    #Record count for each read (4 lines)
    rec_no = 1
    while linecount < numofline:
        if recount == readperFile:
            fileIndex += 1
            if fqb:
                suba.close()
                subb.close()
                fout1 = outDir + '/' + fout + "_1_%s.fastq" %(fileIndex)
                suba = open(fout1, "w")
                fout2 = outDir + '/' + fout + "_2_%s.fastq" %(fileIndex)
                subb = open(fout2, "w")
            else:
                suba.close()
                fout1 = outDir + '/' + fout + "_%s.fastq" %(fileIndex)
                suba = open(fout1, "w")
    
            sys.stderr.write("Writing sequences to file %s...\n" %(fileIndex))
            recount = 0
        
        if rec_no == 4:
            recount += 1
            rec_no = 0
        suba.write(fha.readline())
        if fqb:
            subb.write(fhb.readline())
        rec_no += 1
        linecount += 1
    
    suba.close()
    if fqb:
        subb.close()
    return

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-1', dest='fq1', help='1st pair of read in PER or SER', required = True)
    parser.add_argument('-2', dest='fq2', help='2st pair of read in PER')
    parser.add_argument('-m', dest='mode', help='Mode of split fastq file, 0: number of reads, 1: number of files', required=True)
    parser.add_argument('-fn', dest='fnum', help='Total number of subset files')
    parser.add_argument('-rn', dest='rnum', help='Number of reads in each subset file')
    parser.add_argument('-od', dest='outDir', help='Output Directory (Default=. (current directory))', default = '.')
    parser.add_argument('-pre', dest='pre', help='Prefix of output files (Default=subset)', default = 'subset')
    
    args = parser.parse_args()
    
    if int(args.mode) == 0:
        if not args.rnum:
            sys.stderr.write("'-rn' is required for mode 0\n")
            sys.exit(1)
        sp_reads(args.fq1, args.fq2, int(args.mode), int(args.rnum), args.pre, args.outDir)
        sys.stderr.write("Done!\n")
        sys.exit(0)
    elif int(args.mode) == 1:
        if not args.fnum:
            sys.stderr.write("'-fn' is required for mode 1\n")
            sys.exit(1)
        sp_reads(args.fq1, args.fq2, int(args.mode), int(args.fnum), args.pre, args.outDir)
        sys.stderr.write("Done!\n")
        sys.exit(0)
    else:
        sys.stderr.write("Error: mode can only be 0 or 1\n")
        sys.exit(1)
    parser.print_help()
    sys.exit(1)



