#!/usr/bin/env python
import csv
import sys
import re
from Bio import SeqIO
from Bio import SeqRecord
from Bio.Seq import Seq

def readSourceTemp(tempfile):
    tempinfo = {}
    with open(tempfile, 'r') as fh_temp:
        reader = csv.reader(fh_temp, delimiter="\t")
        header = next(reader)
        header = header[1:]
        for line in reader:
            tempinfo[line[0]] = line[1:]
    return (tempinfo, header)

def readFasta(fasfile, locname, tempinfo, tempheader):
    regex = re.compile("[-]+")
    numfields = len(tempheader)
    with open(fasfile, 'r') as fh_fasfile:
        for seq in SeqIO.parse(fh_fasfile,"fasta"):
            if (seq.id in tempinfo):
                fasseq = regex.sub("", str(seq.seq))
                seqid = seq.id
                for i in range(numfields):
                    seqid += " [%s=%s]" %(tempheader[i],tempinfo[seq.id][i])
                seqid += " Cryptococcus neoformans var. grubii partial %s gene, strain %s." %(locname, seq.id)
                print((">" + seqid))
                print (fasseq)
   
if __name__ == '__main__':
    if (len(sys.argv)) < 4:
        sys.stderr.write ("Usage: <Fasta_File> <Source_Temp> <Locus_Name>")
        sys.exit(1)
    
    fasfile = sys.argv[1]
    tempfile = sys.argv[2]
    locname = sys.argv[3]
    
    tempinfo, tempheader = readSourceTemp(tempfile)
    readFasta(fasfile, locname, tempinfo, tempheader)
    