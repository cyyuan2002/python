#!/usr/bin/env python
"""This script is used to search for variant site in alignments(Fasta Format)"""
from Bio import AlignIO
import sys

Nucleotides = ["a","t","c","g","n","A","T","C","G","N","-"]

def SearchVariance(Aligns):
    """Search variant nuclotide in the alignment"""
    alignlen = Aligns.get_alignment_length()
    lastvariant = 0
    allvariant = []
    variantBase = {}
    for n in range(alignlen):
        base_dict = {}
        for record in Aligns:
            curbase = record.seq[n]
            upbase = curbase.upper()
            if(upbase not in Nucleotides):
                raise ValueError (upbase + "is undefined\n")
            if(curbase not in base_dict):
                base_dict[curbase] = 1
            else:
                base_dict[curbase] += 1

        if(len(base_dict) > 1):
            if(len(base_dict) == 2 and 'N' in base_dict):
                continue
            else:
                bases = []
                for record in Aligns:
                    curbase = record.seq[n]
                    upbase = curbase.upper()
                    if(record.id in variantBase):
                        variantBase[record.id] += upbase
                    else:
                        variantBase[record.id] = upbase

    return variantBase

if(len(sys.argv) < 2):
    print(("Usage: %s <InputFile>" %(sys.argv[0])))
    sys.exit(1)

FileName = sys.argv[1]

fh_file = open(FileName,"rU")
aligns = AlignIO.read(fh_file,"fasta")
fh_file.close()
variants = SearchVariance(aligns)
for seqn in variants:
    print((">"+seqn+"\n"+variants[seqn]))
exit(0)
