#!/usr/bin/env python
##this script is used to convert the depth output from samtools to regions
import csv
import sys
import argparse


class Region:
    chrom = None
    start = None
    end = None
    depth = None

    def __init__(self, chrom, start, end, depth):
        self.chrom = chrom
        self.start = start
        self.end = end
        self.depth = depth

def create_region(filen, inter_len, min_len, min_cov):
    regions = []
    with open(filen, 'rU') as fh:
        reader = csv.reader(fh, delimiter="\t")
        last_chrom = ""
        start = 0
        end = 0
        depth = 0
        for line in reader:
            if line[0] != last_chrom:
                if last_chrom != "":
                    if end - start >= min_len:
                        depth = round(depth/(start-end + 1), ndigits=2)
                        if depth >= min_cov:
                            new_region = Region(chrom=last_chrom, start=start, end=end, depth=depth)
                            regions.append(new_region)
                last_chrom = line[0]
                start = int(line[1])
                end = int(line[1])
                if len(line) < 3:
                    depth = 1
                else:
                    depth = int(line[2])
                continue

            if int(line[1]) > end + inter_len:
                if end - start >= min_len:
                    depth = round(depth / (end - start + 1), ndigits=2)
                    if depth >= min_cov:
                        new_region = Region(chrom=last_chrom, start=start, end=end, depth=depth)
                        regions.append(new_region)
                start = int(line[1])
                end = int(line[1])
                if len(line) < 3:
                    depth = 1
                else:
                    depth = int(line[2])
                continue
            else:
                end = int(line[1])
                if len(line) < 3:
                    depth += 1
                else:
                    depth += int(line[2])
    if end - start >= min_len:
        depth = round(depth / (end - start + 1), ndigits=2)
        if depth >= min_cov:
            new_region = Region(chrom=last_chrom, start=start, end=end, depth=depth)
            regions.append(new_region)

    return regions

def print_regions(regions):
    for region in regions:
        print("%s\t%s\t%s\t%s" %(region.chrom, region.start, region.end, region.depth))

if __name__ == "__main__":
    if len(sys.argv) < 2:
        sys.stderr.write("Usage:%s <depth_file> [interval_length: default 10]\n" % sys.argv[0])
        sys.exit(1)

    filen = sys.argv[1]

    if len(sys.argv) == 3:
        interval_length = int(sys.argv[2])
    else:
        interval_length = 10

    minimal_length = 10
    minimal_coverage = 10

    regions = create_region(filen, interval_length, minimal_length, minimal_coverage)
    print_regions(regions)


