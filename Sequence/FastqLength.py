#!/usr/bin/env python
import sys
from Bio import SeqIO

seqfile = sys.argv[1]
handle = open(seqfile, 'rU')

seqcount = 0
totalseq = 0
seqlens = []

for seq in SeqIO.parse(handle,"fastq-sanger"):
    seqlen = len(seq)
    seqcount += 1
    totalseq += seqlen
    seqlens.append(seqlen)
    print (seqlen)

sortedlength = sorted(seqlens)
N50pos = seqcount/2
N50 = sortedlength[N50pos-1]
averlen = totalseq/seqcount
sys.stderr.write("Number of sequences: %s\n" %(seqcount))
sys.stderr.write("Total sequence length: %s\n" %(totalseq))
sys.stderr.write("Average length: %s\n" %(averlen))
sys.stderr.write("N50 length: %s\n" %(N50))
handle.close()
sys.exit(0)

