#!/usr/bin/env python

import sys
import argparse
import csv

def ReadPropeties(propfile, ped_info, properties):
    props = {}
    fhfile = open(propfile, 'r')
    csvreader = csv.reader(fhfile, delimiter = "\t")
    for line in csvreader:
        props[line[0]] = line[1]
    fhfile.close()

    if(not DictCompare(ped_info, props)):
        raise ValueError ("%s IDs do not equal to IDs in ped file" %(propfile))
    else:
        properties = MergeProperties(properties, props)

    return properties

def AddEmptyProps(properties,number):
    for ID in properties:
        if(properties[ID] is None):
            properties[ID] = str(number)
        else:
            properties[ID] = properties[ID] + "," + str(number)
    return properties

def AddFamilies(propfile, ped_info, properties):
    props = {}
    fhfile = open(propfile, 'r')
    csvreader = csv.reader(fhfile, delimiter = "\t")
    for line in csvreader:
        props[line[0]] = line[1]
    fhfile.close()
    familycounts = {}
    familyID = {}

    if(not DictCompare(ped_info, props)):
        raise ValueError ("%s IDs do not equal to IDs in ped file" %(propfile))
    else:
        for ID in ped_info:
            familyid = props[ID]
            if(familyid in familycounts):
                familycounts[familyid] += 1
            else:
                familycounts[familyid] = 1
            famcount = familycounts[familyid]
            properties[ID] = familyid + "," + str(famcount)
            familyID[ID] = familyid + "," + str(famcount)
        return (properties, familyID)

def DictCompare(dict_a, dict_b):
    diffset = set(dict_a.keys()) - set(dict_a.keys())
    if(diffset):
        return False
    else:
        return True

def MergeProperties(mergedprop, newprop):
    if(len(mergedprop) < 1):
        mergedprop = newprop
    else:
        for ID in mergedprop:
            mergedprop[ID] = mergedprop[ID] + "," + newprop[ID]
    return mergedprop

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-I", dest = 'ped_File', required = True,
                        help="ped File for analysis")
    parser.add_argument("-O", dest = 'Output_File', required = True,
                        help="output file")
    parser.add_argument("-F", dest = 'Family_ID', required = False,
                        help="Family ID")
    parser.add_argument("-P", dest = 'Paternal_ID', required = False,
                        help="Paternal ID")
    parser.add_argument("-M", dest = 'Maternal_ID', required = False,
                        help="Maternal ID")
    parser.add_argument("-S", dest = "Sex", required = False,
                        help="Sex (1=male; 2=female; other=unknown)")
    parser.add_argument("-N", dest = "Phenotype", required = True,
                        help="Phenotype")
    parser.add_argument("-C", dest = "Covariate", required = False,
                        help="Covariate Info")

    if(len(sys.argv) < 2):
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()
    pedfile = args.ped_File
    indifile = args.Family_ID
    patfile = args.Paternal_ID
    matfile = args.Maternal_ID
    sexfile = args.Sex
    phenofile = args.Phenotype
    covafile = args.Covariate
    outfile = args.Output_File

    ped_info = {}
    fh_pedfile = open(pedfile, 'r')
    csv_pedfile = csv.reader(fh_pedfile, delimiter = ' ')
    for line in csv_pedfile:
        ped_info[line[0]] = line[1:]
    fh_pedfile.close()

    properties = dict.fromkeys(list(ped_info.keys()))
    familyid = {}
    if indifile is not None:
        (properties, familyid) = AddFamilies(indifile, ped_info, properties)
    else:
        properties = AddEmptyProps(properties, 1)

    if patfile is not None:
        properties = ReadPropeties(patfile, ped_info, properties)
    else:
        properties = AddEmptyProps(properties, 0)

    if matfile is not None:
        properties = ReadPropeties(matfile, ped_info, properties)
    else:
        properties = AddEmptyProps(properties, 0)

    if sexfile is not None:
        properties = ReadPropeties(sexfile, ped_info, properties)
    else:
        properties = AddEmptyProps(properties, 1)

    if phenofile is not None:
        properties = ReadPropeties(phenofile, ped_info, properties)
    else:
        properties = AddEmptyProps(properties, 1)

    if covafile is not None:
        if len(familyid) < 1:
            raise ValueError("Family information needed!")
        else:
            fh_covaIn = open(covafile, 'r')
            csv_cova = csv.reader(fh_covaIn, delimiter = '\t')
            cova_header = next(csv_cova)
            cova_header.pop(0)
            cova_header[:0] = ["FID", "IID"]
            covInfo = {}
            for line in csv_cova:
                covInfo[line[0]] = line[1:]
            fh_covaIn.close()
            covaOut = outfile + ".cova"
            fh_covaOut = open(covaOut, 'w')
            fh_covaOut.write(" ".join(cova_header) + "\n")
            for ID in familyid:
                famid = familyid[ID].split(',')
                covinfo = covInfo[ID]
                covinfo[:0] = famid
                fh_covaOut.write(" ".join(covinfo) + "\n")
            fh_covaOut.close()

    if len(properties) > 1:
        fh_prop = open(outfile, 'w')
        for ID in properties:
            props = properties[ID].split(',')
            ped = ped_info[ID]
            ped[:0] = props
            if indifile is not None:
                fh_prop.write(" ".join(ped) + "\n")
            else:
                fh_prop.write(ID + " " + " ".join(ped) + "\n")

    sys.exit(0)

if(__name__ == '__main__'):
    main()
