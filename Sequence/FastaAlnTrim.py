#!/usr/bin/env python

import sys
import argparse
from Bio import SeqIO

def readFile(FileName, TrimMode, TrimA, TrimB):
    Seqs = []
    fh_file = open (FileName,'rU')
    for seq in SeqIO.parse(fh_file,"fasta"):
        seqlen = len(seq)
        if TrimMode == 1:
            if(TrimA > 0):
                seqend = seqlen - TrimA
                seq = seq[TrimA:seqend]
        else:
            if TrimB == 0:
                seq = seq[(TrimA-1):]
            else:
                seq = seq[(TrimA-1):-(TrimB)]
        Seqs.append(seq)
    return Seqs


if(__name__ == "__main__"):
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-I", dest = 'Input_file', required = True,
                        type = str, help="input file names")
    parser.add_argument("-T", dest = 'Trim_base', required = False,
                        type = int, help="bases trimmed on the both ends of aligned sequences")
    parser.add_argument("-S", dest = 'Start_site', required = False, type = int,
                        help = "start site of the trimmed sequence")
    parser.add_argument("-E", dest = 'End_site', required = False, type = int,
                        help = "end site of the trimmed sequence")
    parser.add_argument("-O", dest = 'Output_file', required = True,
                        type = str, help="output file name")
    
    if(len(sys.argv) < 2):
        parser.print_help()
        sys.exit(1)
    
    args = parser.parse_args()
    FileName = args.Input_file
    OutFileName = args.Output_file
    TrimBase = args.Trim_base
    TrimS = args.Start_site
    TrimE = args.End_site

    TrimMode = 1
    if(TrimBase is None):
        if TrimS is None and TrimE is None:
            TrimMode = 1
            TrimBase = 0
        else:
            TrimMode = 2
            if TrimS is None:
                TrimS = 1
            if TrimE is None:
                TrimE = 0
    
    if TrimMode == 1:
        AlignSeq = readFile(FileName, 1, TrimBase, 0)
    else:
        AlignSeq = readFile(FileName, 2, TrimS, TrimE)
        
    fh_out = open(OutFileName, 'w')
    for seq in AlignSeq:
        outseq = str(seq.seq)
        #outseq = outseq.replace("-","")
        fh_out.write (">%s\n%s\n" %(seq.id,outseq))
    
    sys.exit(0)
    