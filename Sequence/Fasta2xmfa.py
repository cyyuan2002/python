#!/usr/bin/env python
"""This script is used to convert fasta alignment to xmfa format"""
#pylint: disable-msg=C0103
import sys
from os import path
from Bio import SeqIO

if(len(sys.argv) < 3):
    print ("Usage: %s [-T Num] <File1> <File2> <...>" %(sys.argv[0]))
    sys.exit(1)

TrimBase = 0

if(sys.argv[1] == '-T'):
    if(sys.argv[2].isdigit()):
        TrimBase = int(sys.argv[2])
        FileNames = sys.argv[3:]
    else:
        print ("End trim number must be integer!")
        sys.exit(1)
else:
    FileNames = sys.argv[1:]

for FN in FileNames:
    fh_file = open (FN,'rU')
    FileBase = path.basename(FN)
    FileNs = FileBase.split(".")
    Loci = FileNs[0]
    print ("#Gene: " + Loci)
    for seq in SeqIO.parse(fh_file,"fasta"):
        if(TrimBase > 0):
            seqlen = len(seq)
            seqend = seqlen - TrimBase
            seq = seq[TrimBase:seqend]
        print (seq.format("fasta"), end = "")
    print("=")
    fh_file.close()

