#!/usr/bin/env python3

import sys
from Bio import SeqIO

seqfile = sys.argv[1]
idfile = sys.argv[2]

fh_idfile = open(idfile, 'r')
seqids = []

for line in fh_idfile:
    seqid = line.strip()
    seqids.append(seqid)
fh_idfile.close()

handle = open(seqfile, 'rU')

for seq in SeqIO.parse(handle,"fastq-sanger"):
    if(seq.id in seqids):
        print(seq.format("fastq-sanger"), end = '')

handle.close()
sys.exit(0)

