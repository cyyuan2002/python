#!/usr/bin/env python

import sys
import argparse
import tempfile
from Bio import AlignIO
from numpy import transpose
import ConcAlignedFasta
from os import remove

Nucletides = {"A":1, "a":1, "T":2, "t":2, "C":3, "c":3, "G":4, "g":4, "N":0, "-":0}

def IUPACambiguity(base):
    """..."""
    bases = []
    if(base == 'R'):
        bases = ['A','G']
    elif(base == 'M'):
        bases = ['A','C']
    elif(base == 'W'):
        bases = ['A','T']
    elif(base == 'Y'):
        bases = ['C','T']
    elif(base == 'S'):
        bases = ['C','G']
    elif(base == 'K'):
        bases = ['G','T']
    else:
        bases = 'N'
    return bases

def MergeRows(Row1, Row2):
    newrow = []
    for i in range(len(Row1)):
        newrow.append(Row1[i])
        newrow.append(Row2[i])
    return newrow

def VariantTrans(Bases, RetSite):
    """convert bases to structure numbers in col"""
    transRes = []
    transRes.append(RetSite)
    for base in Bases:
        if(base in Nucletides):
            transRes.append(Nucletides[base])
            transRes.append(Nucletides[base])
        else:
            bases = IUPACambiguity(base)
            if(len(bases) > 1):
                transRes.append(Nucletides[bases[0]])
                transRes.append(Nucletides[bases[1]])
            else:
                transRes.append(Nucletides[bases[0]])
                transRes.append(Nucletides[bases[0]])
    return transRes

def SearchVariance(Aligns, SeqLengths, FileNames):
    """Search variant nuclotide in the alignment"""
    alignlen = Aligns.get_alignment_length()
    lastvariant = 0
    allvariant = []
    seqLens = []
    totallen = 0
    for length in SeqLengths:
        seqLens.append(totallen)
        totallen += length

    seqLens.append(totallen)

    Fregcount = 0
    Variantcount = 0
    for n in range(alignlen):
        if(n >= seqLens[Fregcount+1]):
            Fregcount += 1

        base_dict = {}
        for record in Aligns:
            curbase = record.seq[n]
            upbase = curbase.upper()
            if(upbase not in Nucletides):
                covbases = IUPACambiguity(upbase)
                if(len(covbases) == 1): #Other bases covert to 'N'
                    upbase = 'N'

            if(curbase not in base_dict):
                base_dict[curbase] = 1
            else:
                base_dict[curbase] += 1


        if(len(base_dict) == 2):
            if('N' in base_dict):
                continue
            Variantcount += 1
            #retsite = n - seqLens[Fregcount] + 1
            retsite = n + 1
            siteinfo = str(Fregcount+1) + ",rs" + str(Variantcount) + ",0," + str(retsite)
            bases = []
            for record in Aligns:
                curbase = record.seq[n]
                upbase = curbase.upper()
                bases.append(upbase)
            allvariant.append(VariantTrans(bases, siteinfo))

    return allvariant

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-T", dest = 'Trim_base', required = False, default = 0,
                        type = int, help="bases trimed in the end of alignments")
    parser.add_argument("-O", dest = 'Output_file', required = True,
                        type = str, help="output file name")
    parser.add_argument("-F", dest = 'Input_files', nargs = "+",
                        help="input file names")
    if(len(sys.argv) < 2):
        parser.print_help()
        sys.exit(1)
    args = parser.parse_args()
    TrimBase = args.Trim_base
    OutFileName = args.Output_file
    FileNames = args.Input_files
    (Seqs, SeqLengths) = ConcAlignedFasta.ConcFile(FileNames, TrimBase)
    #AlignOut = StringIO()
    AlignFile = OutFileName + ".aln"
    AlignOut = open(AlignFile,'w')
    for seqID in Seqs:
        seq = Seqs[seqID]
        AlignOut.write(seq.format('fasta'))
    AlignOut.close()
    aligns = AlignIO.read(open(AlignFile,'r'),"fasta")
    #AlignOut.close()
    variants = SearchVariance(aligns, SeqLengths, FileNames)
    transvars = transpose(variants)
    str_transvars = transvars.astype(str)

    OutMapFile = OutFileName + ".map"
    OutPepFile = OutFileName + ".ped"

    fh_outmap = open(OutMapFile, 'w')
    for infor in str_transvars[0]:
        newinfo = infor.replace(",","\t")
        fh_outmap.write (newinfo+"\n")
    #print (" ".join(str_transvars[0])) ##print site row
    fh_outmap.close()

    rowcount = 1
    fh_outpep = open(OutPepFile,'w')

    for record in aligns:
        mergedRow = MergeRows(str_transvars[rowcount], str_transvars[rowcount+1])
        fh_outpep.write (record.id + " " + " ".join(mergedRow) + "\n")
        rowcount += 2

if(__name__ == '__main__'):
    main()

sys.exit(0)
