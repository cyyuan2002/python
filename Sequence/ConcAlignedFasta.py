#!/usr/bin/env python
"""This script is used to concatenate aligned fasta files"""
#pylint: disable-msg=C0103
import sys
from Bio import SeqIO


def ConcFile(FileNames, TrimBase):
    Seqs = {}
    SeqIds = []
    LastFile = ""
    SeqLengths = []
    SeqFiles = []
    for FN in FileNames:
        fh_file = open (FN,'rU')
        currIds = []
        for seq in SeqIO.parse(fh_file,"fasta"):
            currIds.append(seq.id)
            if(FN not in SeqFiles):
                seqlen = len(seq) - TrimBase
                SeqLengths.append(seqlen)
                SeqFiles.append(FN)
            if(TrimBase > 0):
                seqlen = len(seq)
                seqend = seqlen - TrimBase
                seq = seq[TrimBase:seqend]
            if(seq.id not in Seqs):
                Seqs[seq.id] = seq
            else:
                Seqs[seq.id] += seq

        if(LastFile == ""):
            SeqIds = currIds
        else:
            if(len(SeqIds) != len(currIds)):
                if(len(SeqIds) > len(currIds)):
                    diffIds = list(set(SeqIds) - set(currIds))
                    sys.stderr.write("Warning: " + ",".join(diffIds) + \
                                     "could not be found in file: " + FN + "\n")
                else:
                    diffIds = list(set(currIds) - set(SeqIds))
                    sys.stderr.write("Warning: " + ",".join(diffIds) + "could not \
                                     be found in file: " + LastFile + "\n")
                    LastFile = FN
                    SeqIds = currIds
            else:
                LastFile = FN

        fh_file.close()

    #for seqID in Seqs:
    #    seq = Seqs[seqID]
    #    print(seq.format('fasta'), end='')

    return (Seqs, SeqLengths)


def main():
    if(len(sys.argv) < 3):
        print ("Usage: %s [-T Num] <File1> <File2> <...>" %(sys.argv[0]))
        sys.exit(1)

    TrimBase = 0

    if(sys.argv[1] == '-T'):
        if(sys.argv[2].isdigit()):
            TrimBase = int(sys.argv[2])
            FileNames = sys.argv[3:]
        else:
            print ("End trim number must be integer!")
            sys.exit(1)
    else:
        FileNames = sys.argv[1:]

    (Seqs, SeqLengths) = ConcFile(FileNames, TrimBase)
    for seqID in Seqs:
        seq = Seqs[seqID]
        print(seq.format('fasta'), end='')

if(__name__ == '__main__'):
    main()
    sys.exit(0)


