#!/usr/bin/env python
import random
import sys
import argparse


def pe_random_records(fqa, fqb, fout, N):
    """ get N random headers from a fastq file without reading the
    whole thing into memory"""
    records = sum(1 for _ in open(fqa)) / 4
    rand_records = sorted([random.randint(0, records - 1) for _ in range(N)])

    fha, fhb = open(fqa),  open(fqb)

    fout1 = fout + "_1.fastq"
    fout2 = fout + "_2.fastq"
    suba, subb = open(fout1, "w"), open(fout2, "w")

    sys.stderr.write("Writing sequences to %s and %s...\n" % (fout1, fout2))

    rec_no = - 1
    for rr in rand_records:
        while rec_no < rr:
            rec_no += 1
            for i in range(4):
                fha.readline()
            for i in range(4):
                fhb.readline()
        for i in range(4):
            suba.write(fha.readline())
            subb.write(fhb.readline())
        rec_no += 1


def se_random_records(fin, fout, N):
    """ get N random headers from a fastq file without reading the
    whole thing into memory"""
    records = sum(1 for _ in open(fin)) / 4
    rand_records = sorted([random.randint(0, records - 1) for _ in range(N)])

    fha = open(fin)
    sys.stderr.write("Writing sequences to %s.fastq\n" % (fout))
    suba = open(fout + ".fastq", "w")
    rec_no = - 1
    for rr in rand_records:
        while rec_no < rr:
            rec_no += 1
            for i in range(4):
                fha.readline()
        for i in range(4):
            suba.write(fha.readline())
        rec_no += 1

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-1', dest='fq1', help='1st pair of read in PER or SER', required=True)
    parser.add_argument('-2', dest='fq2', help='2st pair of read in PER')
    parser.add_argument('-o', dest='fout',
                        help='Prefix for output file', required=True)
    parser.add_argument(
        '-n', dest='num', help='Number of reads required', default=1000000, type=int)

    args = parser.parse_args()

    if args.fq2:
        pe_random_records(args.fq1, args.fq2, args.fout, args.num)
        sys.stderr.write("Done!\n")
        sys.exit(0)
    else:
        se_random_records(args.fq1, args.fout, args.num)
        sys.stderr.write("Done!\n")
        sys.exit(0)

    parser.print_help()
    sys.exit(1)
