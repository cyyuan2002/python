#!/usr/bin/env python

import csv
import sys
import argparse
import numpy as np
from os.path import isfile
from Bio.Alphabet import IUPAC, Gapped
from Bio import AlignIO
from Bio.Align import MultipleSeqAlignment as MultiAlign
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq

def readAlleleType(AltypeFile):
    fhfile = open(AltypeFile, 'rU')
    reader = csv.reader(fhfile, delimiter='\t')
    alleleSeq = {}
    alleleIso = {}
    for line in reader:
        if(line[1] not in alleleSeq):
            alleleSeq[line[1]] = line[2]
            tmpisonames = []
            tmpisonames.append(line[0])
            alleleIso[line[1]] = tmpisonames
        else:
            tmpisonames = alleleIso[line[1]]
            tmpisonames.append(line[0])
            alleleIso[line[1]] = tmpisonames
    return (alleleSeq, alleleIso)

def readGroupInfo(GroupFile):
    fhfile = open(GroupFile, 'rU')
    reader = csv.reader(fhfile, delimiter='\t')
    isogroup = {}
    for line in reader:
        isogroup[line[0]] = line[1]
    
    return (isogroup)

def buildNexusInfo(AlleleSeq,AlleleIso,IsoGroup):
    SeqAlignment = MultiAlign(None,alphabet=Gapped(IUPAC.ambiguous_dna))
    SortedSeqs = sorted(AlleleSeq, key = int)
    for seqId in SortedSeqs:
        seqrecid = "Hap_" + seqId
        seq = Seq(AlleleSeq[seqId],Gapped(IUPAC.ambiguous_dna))
        seqrec = SeqRecord(seq,id = seqrecid)
        SeqAlignment.append(seqrec)
    
    groupset = sorted(set(IsoGroup.values()), key = str)
    
    zeros = [0] * len(groupset)
    AlleleNum = {}
    for iso in AlleleIso:
        isogroup = AlleleIso[iso]
        groupnum = zeros[:]
        for isoname in isogroup:
            groupidx = groupset.index(IsoGroup[isoname])
            groupnum[groupidx] += 1
        seqrecid = "Hap_" + iso
        AlleleNum[seqrecid] = groupnum
    
    return (SeqAlignment, AlleleNum, groupset)
    
def AlerquinOut(SeqAlignment, AlleleNum, GroupSet, Outfile):
    hapfile = Outfile + ".hap"
    arpfile = Outfile + ".arp"
    fh_hap = open(hapfile, 'w')
    for seq in SeqAlignment:
        fh_hap.write ("%s   %s\n\n" %(seq.id,str(seq.seq)))
    fh_hap.close()
    
    fh_arp = open(arpfile, 'w')
    profileInfo = """
[Profile]
   Title = "Haplotype Data from Conc.all.noRef.aln.fas.nex DnaSP file"
   NbSamples = %s
   DataType = DNA
   GenotypicData = 0
   LocusSeparator = NONE
   MissingData = "?"
   CompDistMatrix = 1
""" %(len(GroupSet))
    
    fh_arp.write(profileInfo)
    fh_arp.write('[Data]\n\n[[HaplotypeDefinition]]\n    HaplList = EXTERN "%s"\n\n' %(hapfile))
    fh_arp.write("[[Samples]]\n")
    
    for i in range(len(GroupSet)):
        samplename = GroupSet[i]
        samplesize = 0
        hapinfos = []
        for seqid in AlleleNum:
            groupnum = AlleleNum[seqid]
            if groupnum[i] > 0:
                samplesize += groupnum[i]
                hapinfo = seqid + " " + str(groupnum[i])
                hapinfos.append(hapinfo)
        fh_arp.write('   SampleName = "%s"\n   SampleSize = %s\n   SampleData = {\n' %(samplename,samplesize))
        for info in hapinfos:
            fh_arp.write ('      %s\n' %(info))
        fh_arp.write('}\n\n')
    fh_arp.close()
    
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Convert allele type file into nexus file:')
    parser.add_argument('-i', dest='input_file', help='allele type file')
    parser.add_argument('-g', dest='group_file', help='group file')
    parser.add_argument('-o', dest='output_file', help='output file')
    
    args = parser.parse_args()
    
    if(len(sys.argv)<3):
        parser.print_help()
        sys.exit(1)
    
    infile = args.input_file
    groupfile = args.group_file
    outfile = args.output_file
    
    AlleleSeq, AlleleIso = readAlleleType(infile)
    IsoGroup = readGroupInfo(groupfile)
    SeqAlignment, AlleleNum, GroupSet = buildNexusInfo(AlleleSeq,AlleleIso,IsoGroup)
    AlerquinOut(SeqAlignment, AlleleNum, GroupSet, outfile)
    sys.exit(0)

    