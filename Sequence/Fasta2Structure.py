#!/usr/bin/env python
"""This script is used to convert fasta alignment to structure format"""
#pylint: disable-msg=C0103
import sys
from Bio import AlignIO
from numpy import transpose

Nucletides = {"A":0, "a":0, "T":1, "t":1, "C":2, "c":2, "G":3, "g":3, "N":-9}

def IUPACambiguity(base):
    """..."""
    bases = []
    if(base == 'R'):
        bases = ['A','G']
    elif(base == 'M'):
        bases = ['A','C']
    elif(base == 'W'):
        bases = ['A','T']
    elif(base == 'Y'):
        bases = ['C','T']
    elif(base == 'S'):
        bases = ['C','G']
    elif(base == 'K'):
        bases = ['G','T']
    else:
        bases = 'N'
    return bases

def VariantTrans(Bases, RetSite):
    """convert bases to structure numbers in col"""
    transRes = []
    transRes.append(RetSite)
    for base in Bases:
        if(base in Nucletides):
            transRes.append(Nucletides[base])
            transRes.append(Nucletides[base])
        else:
            bases = IUPACambiguity(base)
            if(len(bases) > 1):
                transRes.append(Nucletides[bases[0]])
                transRes.append(Nucletides[bases[1]])
            else:
                transRes.append(Nucletides[bases[0]])
                transRes.append(Nucletides[bases[0]])
    return transRes

def SearchVariance(Aligns):
    """Search variant nuclotide in the alignment"""
    alignlen = Aligns.get_alignment_length()
    lastvariant = 0
    allvariant = []
    for n in range(alignlen):
        base_dict = {}
        for record in Aligns:
            curbase = record.seq[n]
            upbase = curbase.upper()
            if(upbase not in Nucletides):
                covbases = IUPACambiguity(upbase)
                if(len(covbases) == 1): #Other bases covert to 'N'
                    upbase = 'N'

            if(curbase not in base_dict):
                base_dict[curbase] = 1
            else:
                base_dict[curbase] += 1

        if(len(base_dict) > 1):
            if(len(base_dict) == 2 and 'N' in base_dict):
                continue
            else:
                retsite = n - lastvariant
                lastvariant = n
                bases = []
                for record in Aligns:
                    curbase = record.seq[n]
                    upbase = curbase.upper()
                    bases.append(upbase)
                allvariant.append(VariantTrans(bases, retsite))
        elif(n == 0):
            bases = []
            for record in Aligns:
                curbase = record.seq[n]
                upbase = curbase.upper()
                bases.append(upbase)
            allvariant.append(VariantTrans(bases, -1))

    return allvariant

def main():
    if(len(sys.argv) < 2):
        print(("Usage: %s <InputFile>" %(sys.argv[0])))
        sys.exit(1)

    FileName = sys.argv[1]

    fh_file = open(FileName,"rU")
    aligns = AlignIO.read(fh_file,"fasta")
    fh_file.close()
    variants = SearchVariance(aligns)
    transvars = transpose(variants)
    str_transvars = transvars.astype(str)
    print((" ".join(str_transvars[0]))) ##print site row
    rowcount = 1
    for record in aligns:
        print((record.id + " " + " ".join(str_transvars[rowcount])))
        print((record.id + " " + " ".join(str_transvars[rowcount+1])))
        rowcount += 2

if(__name__ == '__main__'):
    main()
    sys.exit(0)
