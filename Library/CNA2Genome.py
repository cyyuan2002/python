#!/usr/bin/env python

class CNA2Genome:
    
    def __init__(self):
        self.chroms = self.__build_chroms()
        self.centromeres = self.__build_centromeres()
    
    def __build_chroms(self):
        genome = GenomeInfo()
        genome.add_chrom_info("supercont2.1", 2291499, "chr1")
        genome.add_chrom_info("supercont2.2", 1621675, "chr2")
        genome.add_chrom_info("supercont2.3", 1575141, "chr3")
        genome.add_chrom_info("supercont2.4", 1084805, "chr4")
        genome.add_chrom_info("supercont2.5", 1814975, "chr5")
        genome.add_chrom_info("supercont2.6", 1422463, "chr6")
        genome.add_chrom_info("supercont2.7", 1399503, "chr7")
        genome.add_chrom_info("supercont2.8", 1398693, "chr8")
        genome.add_chrom_info("supercont2.9", 1186808, "chr9")
        genome.add_chrom_info("supercont2.10", 1059964, "chr10")
        genome.add_chrom_info("supercont2.11", 1561994, "chr11")
        genome.add_chrom_info("supercont2.12", 774062, "chr12")
        genome.add_chrom_info("supercont2.13", 756744, "chr13")
        genome.add_chrom_info("supercont2.14", 926563, "chr14")
        return genome
    
    def __build_centromeres(self):
        centromere = GenomeRegions(self.chroms)
        centromere.add_region_info("supercont2.1","970001","1004000")
        centromere.add_region_info("supercont2.2","866001","893000")
        centromere.add_region_info("supercont2.3","1370501","1410000")
        centromere.add_region_info("supercont2.4","709001","748000")
        centromere.add_region_info("supercont2.5","1562501","1580000")
        centromere.add_region_info("supercont2.6","784001","821000")
        centromere.add_region_info("supercont2.7","526001","568000")
        centromere.add_region_info("supercont2.8","454001","505500")
        centromere.add_region_info("supercont2.9","802001","840500")
        centromere.add_region_info("supercont2.10","199501","230500")
        centromere.add_region_info("supercont2.11","871501","929500")
        centromere.add_region_info("supercont2.12","139501","170500")
        centromere.add_region_info("supercont2.13","579501","632000")
        centromere.add_region_info("supercont2.14","445001","461500")
        return centromere

class GenomeInfo:
    """Genome class for storing all chromosomes information"""
    def __init__(self):
        self.chroms = []

    def __getitem__(self, index):
        return self.chroms[index]

    def __len__(self):
        return len(self.chroms)

    def __eq__(self, other):
        if(not isinstance(other, self.__class__)):
            return False
        if(len(self) != len(other)):
            return False
        for i in range(0, len(self)):
            if(self[i] != other[i]):
                return False
        return True

    def add_chrom(self, cInfo):
        """add new chromsome to genome"""
        self.chroms.append(cInfo)

    def add_chrom_info(self, cName, cLength, cDes = ""):
        """add new chromsome to genome"""
        chrom = ChromInfo(cName, cLength, cDes)
        self.chroms.append(chrom)

    def get_chrom_length(self, cName):
        """return length of specific chromsome"""
        for chrom in self.chroms:
            if(chrom.name == cName):
                return chrom.length
        raise ValueError ("Cannot find chromsomes named %s" %(cName))

    def get_chrom_desc(self, cName):
        """return description of specific chromsome"""
        for chrom in self.chroms:
            if(chrom.name == cName):
                return chrom.desname
        raise ValueError ("Cannot find chromsomes named %s" %(cName))
        
    def get_chrom_names(self):
        """return a list contains all the chrom names"""
        cnames = []
        for chrom in self.chroms:
            cnames.append(chrom.name)
        return cnames

    def get_chrom_lengths(self):
        """return a list contains all the chrom length"""
        clength = []
        for chrom in self.chroms:
            clength.append(chrom.length)
        return clength

    def get_chrom_descs(self):
        """return a list contains all the chrom description"""
        cdesc = []
        for chrom in self.chroms:
            cdesc.append(chrom.desname)
        return cdesc

    def get_longest_chrom(self):
        """return the length of the longest chromsome"""
        maxlength = 0
        for chrom in self.chroms:
            if(chrom.length > maxlength):
                maxlength = chrom.length
        return maxlength

    def check_chrom_region(self, cRegion):
        """check chromsome region in genome"""
        blchrom = False
        for chrom in self.chroms:
            if(cRegion.chrom == chrom.name):
                blchrom = True
        if(not blchrom):
            return False

        chromlen = self.get_chrom_length(cRegion.chrom)
        if(cRegion.spos < 0 or cRegion.epos > chromlen):
            return False
        return True


class ChromInfo(object):
    """class for chromsome information storage"""
    def __init__(self, cName, cLength, cDes = ""):
        self.name = cName
        self.length = int(cLength)
        if(cDes == ""):
            self.desname = cName
        else:
            self.desname = cDes

    def __eq__(self, other):
        if(not isinstance(other, self.__class__)):
            return False
        if(self.name != other.name):
            return False
        if(self.length != other.length):
            return False
        return True

class RegionInfo(object):
    """class for genome region storage"""

    def __init__(self, cChrom, cStart, cEnd, cDir = "+", cDes = ""):
        self.chrom = cChrom
        self.spos = int(cStart)
        self.epos = int(cEnd)
        self.dir = cDir
        self.des = cDes

    def reg_len(self):
        """return length of the region"""
        reglen = self.epos - self.spos + 1
        return reglen

    def reg_info(self):
        infotxt = ("%s:%s-%s" %(self.chrom, self.spos, self.epos))
        return infotxt