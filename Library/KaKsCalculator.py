#!/usr/bin/env python
import sys

class DnDsCalculator:
    def __init__(self, seq1, seq2):
        self.Seq1 = seq1
        self.Seq2 = seq2
        if(len(self.Seq1) != len(self.Seq2)):
            raise ValueError ("Sequences have different length")
        self.SeqLength = len(seq1)
        self.dN = 0
        self.dS = 0
        self._subSites = []
        self._aminoChg = []
        self._subTypes = []
        self._calKaKs()
    
    def getdNdS(self):
        if(self.dS == 0):
            return float('NaN')
        else:
            return float(self.dN)/self.dS
    
    def getSubSites(self):
        return ','.join(self._subSites)
    
    def getAminoChanges(self):
        return ','.join(self._aminoChg)
    
    def getSubTypes(self):
        return ','.join(self._subTypes)
    
    def getSYNSites(self):
        synsites = []
        for i in range(len(self._subTypes)):
            if(self._subTypes[i] == "SYN"):
                synsites.append(str(self._subSites[i]))
        if(len(synsites) < 1):
            return ("NA")
        else:
            return (','.join(synsites))
    
    def getNSYSites(self):
        nsysites = []
        for i in range(len(self._subTypes)):
            if(self._subTypes[i] == "NSY"):
                nsysites.append(str(self._subSites[i]))
        if(len(nsysites) < 1):
            return ("NA")
        else:
            return(','.join(nsysites))
    
    def getNONSites(self):
        nonsites = []
        for i in range(len(self._subTypes)):
            if(self._subTypes[i] == "NON"):
                nonsites.append(str(self._subSites[i]))
        if(len(nonsites) < 1):
            return ("NA")
        else:
            return(','.join(nonsites))
    
    def _calKaKs(self):
        for n in range(0,len(self.Seq1),3):
            codon1 = self.Seq1[n:n+3]
            codon2 = self.Seq2[n:n+3]
            if (codon1 == codon2):
                continue
            else:
                amino1 = self._transCodon(codon1)
                amino2 = self._transCodon(codon2)
                subtype = ""
                amichg = amino1 + '->' + amino2
                if(amino1 == amino2):
                    self.dS += 1
                    subtype = "SYN"
                else:
                    self.dN += 1
                    self._aminoChg.append(amichg)
                    if(amino2 == '_'):
                        subtype = "NON"
                    else:
                        subtype = "NSY"
                self._subSites.append((n+3)/3)
                self._subTypes.append(subtype)
                
                
    def _transCodon(self, codon):
        codontable = {
        'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
        'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
        'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
        'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',
        'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
        'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
        'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
        'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
        'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
        'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
        'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
        'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
        'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
        'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
        'TAC':'Y', 'TAT':'Y', 'TAA':'_', 'TAG':'_',
        'TGC':'C', 'TGT':'C', 'TGA':'_', 'TGG':'W',
        }
        if(codon not in codontable):
            raise ValueError ('Codon %s is not in codon table' %(codon))
        
        return (codontable[codon])

if __name__ == '__main__':
    infile = sys.argv[1]
    fh_file = open(infile, 'rU')
    linecount = 0
    seqID = ""
    seq1 = ""
    seq2 = ""
    print ("#GeneID\tdN\tdS\tNSY_Sites\tSYN_Sites\tNON_Sites\tdN_Changes")
    for line in fh_file:
        line = line.strip()
        linecount += 1
        if(line == ""):
            linecount = 0
            continue
        else:
            if(linecount == 1):
                seqID = line
            elif(linecount == 2):
                seq1 = line
            elif(linecount == 3):
                seq2 = line
                dnds = DnDsCalculator(seq1, seq2)
                print(("%s\t%s\t%s\t%s\t%s\t%s\t%s" %(seqID, dnds.dN, dnds.dS, dnds.getNSYSites(), dnds.getSYNSites(), dnds.getNONSites(), dnds.getAminoChanges())))
    fh_file.close()
    
    