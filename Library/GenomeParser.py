#!/usr/bin/env python
import GFFParser
from Bio import SeqIO
import sys
#from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
#from Bio.Alphabet import IUPAC

class GenomeParser(object):
    ## gff is GFFParser
    ## genome is SeqIO
    def __init__(self, genome = None, gffparser = None):
        self.genome = genome
        self.gffparser = gffparser
        self.datacheck = False
    
    def readFasta(self, gFile):
        self.genome = self._loadFasta(gFile)
    
    def readGFF(self,gffFile):
        self.gffparser = GFFParser.GFFParser()
        self.gffparser.open(gffFile)
        
    def getGeneSeq(self, gid):
        self._datacheck()
        gene = self._getGene(gid)
        if gene:
            generegion = GFFParser.DNA_Region(gene.s,gene.e,gene.d)
            seq = self._getRegionSeqs(gene.chrom, [generegion])  
            return seq
        return None
    
    def getRNASeq(self, gid):
        self._datacheck()
        gene = self._getGene(gid)
        if gene:
            rna = gene.longestRNA()
            seq = self._getRegionSeqs(gene.chrom, rna.exons)
            return seq
        return None
    
    def getCDSSeq(self, gid):
        self._datacheck()
        gene = self._getGene(gid)
        if gene:
            rna = gene.longestRNA()
            seq = self._getRegionSeqs(gene.chrom, rna.CDSs)
            return seq
        return None
    
    def getProteinSeq(self, gid):
        self._datacheck()
        gene = self._getGene(gid)
        if gene:
            rna = gene.longestRNA()
            seq = self._getRegionSeqs(gene.chrom, rna.CDSs)
            proseq = seq.translate()
            return proseq
        return None
     
    def _datacheck(self):
        if not self.datacheck:
            if not self.genome:
                raise ValueError ("No genome sequence loaded")
            else:
                if not isinstance(self.genome, dict):
                    raise ValueError ("Genome is not dict")
            if not self.gffparser:
                raise ValueError ("No GFF information loaded")
            else:
                if not isinstance(self.gffparser, GFFParser.GFFParser):
                    raise ValueError ("GFF is not GFFParser.GFFParser")
            self._chromCheck()
            self.datacheck = True 
        
    def _getRegionSeqs(self, chrom, regions):
        chromseq = self.genome[chrom]
        seq = None
        for rgn in regions:
            seqa = chromseq[rgn.s-1:rgn.e]
            if not seq:
                seq = seqa
            else:
                seq += seqa
        if regions[0].d == '-':
            seq = seq.reverse_complement()
        return seq
    
    def _getGene(self, gid):
        if not self.gffparser:
            raise ValueError ("No GFFParser found\n")
        gene = self.gffparser.getGene(gid)
        return gene
    
    def _chromCheck(self):
        for chrom in self.gffparser:
            if chrom not in self.genome:
                raise ValueError ("Cannot find %s in genome sequences" %(chrom))
    
    def _loadFasta(self, gfile):
        fh = open(gfile,'r')
        genome = {}
        for seq in SeqIO.parse(fh,"fasta"):
            genome[seq.id] = seq.seq
        fh.close()
        return genome
   
if __name__ == '__main__':
    GFFFile = sys.argv[1]
    GenomeFile = sys.argv[2]
    gene = "CNAG_04548"
    gparser = GenomeParser()
    gparser.readFasta(GenomeFile)
    gparser.readGFF(GFFFile)
    cds = gparser.getCDSSeq(gene)
    rna = gparser.getRNASeq(gene)
    print ("CDS")
    print (cds)
    print ("RNA")
    print (rna)
    