#!/usr/bin/env python
"""This class is used to run GO enrichment using R"""
from os import path
from rpy2.robjects.packages import SignatureTranslatedAnonymousPackage as STAP 
import os
import sys
import csv
import rpy2.robjects as ro
from rpy2.robjects.packages import importr

def goenrich(geneList,allGenes,GOTable,Filename,GraphyTopNode = 5,GenTableTopNode = 10):
    Path = path.dirname(__file__)
    RSource = Path + "/GOEnrich.R"
    with open(RSource,"r") as f: 
        string = ''.join(f.readlines()) 
    r_GOEnrichSource = STAP(string,"goenrich") 
    
    if(not path.isfile(GOTable)):
        print(("Error: Cannot open file: %s!" %(GOTable)))
        sys.exit(1)
    rpkg_topGO = importr("topGO")
    r_GOtable = rpkg_topGO.readMappings(GOTable)
    r_geneList = ro.StrVector(geneList)
    r_allGenes = ro.StrVector(allGenes)
    r_GOEnrichSource.goenrich(r_allGenes,r_geneList,r_GOtable,Filename,GraphyTopNode,GenTableTopNode)
    return

def main(argv):
    if(len(argv) < 2):
        print(("Usage:%s <GOTable> <GeneList> [AllGeneList]" %(argv[0])))
        sys.exit(1)

    GeneList = []
    AGList = []

    GOTable = argv[1]
    GLFile = argv[2]
    AGLFile = ""
    if(argv == 4):
        AGLFile = argv[3]

    fh_gl = open(GLFile, 'r')
    for line in fh_gl:
        GeneList.append(line.strip())
    fh_gl.close

    if(AGLFile != ""):
        fh_agl = open(AGLFile, 'r')
        for line in fh_agl:
            AGList.append(line.strip())
        fh_agl.close()
    else:
        fh_gotable = open(GOTable,'r')
        reader = csv.reader(fh_gotable, delimiter = "\t")
        for line in reader:
            AGList.append(line[0])
        fh_gotable.close()
    
    goenrich(GeneList,AGList,GOTable,GLFile,GraphyTopNode = 5,GenTableTopNode = 10)
    
if(__name__ == "__main__"):
    main(sys.argv)