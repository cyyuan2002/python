#!/usr/bin/env python
"""This class is used to visualize whole genome information based on chroms"""

from collections import Sequence
import cairo
import sys
import colorsys
from math import log10

class CNA2Genome(object):
    
    def __init__(self):
        self.chroms = self.__build_chroms()
        self.centromeres = self.__build_centromeres()
    
    def __build_chroms(self):
        genome = GenomeInfo()
        genome.add_chrom_info("supercont2.1", 2291499, "chr1")
        genome.add_chrom_info("supercont2.2", 1621675, "chr2")
        genome.add_chrom_info("supercont2.3", 1575141, "chr3")
        genome.add_chrom_info("supercont2.4", 1084805, "chr4")
        genome.add_chrom_info("supercont2.5", 1814975, "chr5")
        genome.add_chrom_info("supercont2.6", 1422463, "chr6")
        genome.add_chrom_info("supercont2.7", 1399503, "chr7")
        genome.add_chrom_info("supercont2.8", 1398693, "chr8")
        genome.add_chrom_info("supercont2.9", 1186808, "chr9")
        genome.add_chrom_info("supercont2.10", 1059964, "chr10")
        genome.add_chrom_info("supercont2.11", 1561994, "chr11")
        genome.add_chrom_info("supercont2.12", 774062, "chr12")
        genome.add_chrom_info("supercont2.13", 756744, "chr13")
        genome.add_chrom_info("supercont2.14", 926563, "chr14")
        return genome
    
    def __build_centromeres(self):
        centromere = GenomeRegions(self.chroms)
        centromere.add_region_info("supercont2.1","970001","1004000")
        centromere.add_region_info("supercont2.2","866001","893000")
        centromere.add_region_info("supercont2.3","1370501","1410000")
        centromere.add_region_info("supercont2.4","709001","748000")
        centromere.add_region_info("supercont2.5","1562501","1580000")
        centromere.add_region_info("supercont2.6","784001","821000")
        centromere.add_region_info("supercont2.7","526001","568000")
        centromere.add_region_info("supercont2.8","454001","505500")
        centromere.add_region_info("supercont2.9","802001","840500")
        centromere.add_region_info("supercont2.10","199501","230500")
        centromere.add_region_info("supercont2.11","871501","929500")
        centromere.add_region_info("supercont2.12","139501","170500")
        centromere.add_region_info("supercont2.13","579501","632000")
        centromere.add_region_info("supercont2.14","445001","461500")
        return centromere

class VisualGenome(object):

    def __init__(self):
        self.__regionsets = None
        self.__pointsets = None
        self.__centromere = None
        self.__chroms = None
        self.__colors = []
        self.__chrom_x_coords = {}
        self.__width = 1024
        self.__height = 800
        self.__surface = None
        self.__ctx = None
        self.__border = 0.05
        self.__labelpix = 20
        self.__unit = 1000000
        self.__linecolor = "000000"
        self.__chromcolor = "4A708B"
        self.__centrocolor = "525252"
        self.__chromwidth = 5
        self.__centrowidth = 1
        self.__autochromcolor = False
        self.__autoregioncolor = False
        self.__chromcolors = []
        self.__regioncolors = []
        self.__regioncolor = "5CACEE"
        self.__outfile = "output.pdf"
        self.__outfiletype = "pdf"
        self.__perbppix = 0
        self.__axesspace = 60 #space between axis and image border
        self.__regionspace = 3 #space between regions
        self.__regionchromspace = 20 #space between region and chrom
        self.__chromspace = 0 #obtained by chrom number

    def set_image_size(self, width, height):
        """set output image size by width and height"""
        self.__width = width
        self.__height = height

    def set_regions(self, RegionSets):
        self.__regionsets = RegionSets
        self.__chroms = RegionSets.get_genome_info()
    
    def set_centromere(self,Centromeres):
        self.__centromere = Centromeres
    
    def set_unit(self,Unit):
        self.__unit = Unit
        
    def set_auto_chrom_color(self,isAuto):
        self.__autochromcolor = isAuto
    
    def set_auto_region_color(self,isAuto):
        self.__autoregioncolor = isAuto
        
    def set_output_filetype(self, filetype):
        self.__outfiletype = filetype
        
    def set_output_file(self,filename):
        self.__outfile = filename
        
    def __hex2rgb(self, value):
        value = value.lstrip('#')
        lv = len(value)
        a = list(int(value[i:i+lv//3], 16) for i in range(0, lv, lv//3))
        return([x/255 for x in a])
    
    def __format_color(self,color):
        if(isinstance(color,list)):
            return color
        else:
            color = self.__hex2rgb(color)
            return color
    
    def __color_generator(self,ColorNumber):
        colorchange = 1/ColorNumber
        colors = []
        for i in range(ColorNumber):
            color = list(colorsys.hsv_to_rgb(i*colorchange,1,0.6))
            colors.append(color)
        return colors
    
    def __init_draw(self):
        if(self.__outfiletype == "pdf"):
            self.__surface = cairo.PDFSurface (self.__outfile, self.__width, self.__height)
        self.__ctx = cairo.Context(self.__surface)

    def __end_draw(self):
        if(self.__outfiletype == "pdf"):
            self.__surface.finish()

    def __draw_ticks(self):
        max_ChromLength = self.__chroms.get_longest_chrom()
        unitNumber = max_ChromLength/(self.__unit/10)
        tickwidth1 = 5
        tickwidth2 = 10
        wborder = self.__width * self.__border
        hborder = self.__height * self.__border
        top = hborder + self.__labelpix
        bottom = self.__height - hborder
        ticklength = bottom - top + 1
        unitlength = ticklength / unitNumber
        self.__perbppix = ticklength/max_ChromLength
        txt_unit = "1x10"
        supernumber = str(int(log10(self.__unit)))
        color = self.__format_color(self.__linecolor)
        self.__ctx.set_source_rgb(*color)
        self.__ctx.move_to(wborder,top-20)
        self.__ctx.set_font_size(14)
        x_off, y_off, tw, th = self.__ctx.text_extents(txt_unit)[:4]
        self.__ctx.show_text(txt_unit)
        self.__ctx.set_font_size(12)
        self.__ctx.move_to(wborder+tw+3,top-20-th/2)
        self.__ctx.show_text(supernumber)
        self.__ctx.set_font_size(14)
        self.__ctx.move_to(wborder - 20, (top + bottom)/2)
        self.__ctx.show_text("bp")
        self.__ctx.move_to(wborder,top)
        self.__ctx.line_to(wborder,bottom)
        self.__ctx.set_line_width(1)
        self.__ctx.stroke()
        for i in range(0,int(unitNumber)+1):
            ylabel = str(round((i * 0.1),1))
            relpos = i * unitlength
            relpos = round (relpos,1)
            tick_y = top + relpos
            tick_x_s = wborder
            tick_x_e = wborder
            if(i/5 == int(i/5)):
                tick_x_e += tickwidth2
                self.__ctx.set_font_size(14)
                self.__ctx.move_to(tick_x_e+2,tick_y+4)
                self.__ctx.show_text(ylabel)
            else:
                tick_x_e += tickwidth1
                self.__ctx.set_font_size(12)
                self.__ctx.move_to(tick_x_e+2,tick_y+4)
                self.__ctx.show_text(ylabel)
            self.__ctx.move_to(tick_x_s,tick_y)
            self.__ctx.line_to(tick_x_e,tick_y)
            
        self.__ctx.set_line_width(0.2)
        self.__ctx.stroke()

    def __draw_chroms(self):
        chrom_num = len(self.__chroms)
        chrom_x_shift = 10
        isMulticolor = False
        chromColors = []
        chromColor = []
        if(self.__autochromcolor == False):
            if(len(self.__chromcolors) < 1):
                chromColor = self.__chromcolor
            else:
                if(len(self.__chromcolors) == chrom_num):
                    chromColors = self.__chromcolors
                    isMulticolor = True
                else:
                    raise ValueError ("number of chrom colors is not equal to chrom number")
        else:
            chromColors = self.__color_generator(chrom_num)
            isMulticolor = True

        totalwidth = self.__width - 2*(self.__width * self.__border) - self.__axesspace
        chromspace = round((totalwidth / chrom_num),1)
        self.__chromspace = chromspace
        chrom_x = self.__width * self.__border + self.__axesspace
        chrom_y = hborder = self.__height * self.__border + self.__labelpix
        chrom_x_coords = {}
        for i in range(len(self.__chroms)):
            color = []
            if(isMulticolor):
                color = chromColors[i]
            else:
                color = chromColor
            color = self.__format_color(color)
            chrom = self.__chroms[i]
            chrom_x_s = chrom_x + i * chromspace + chrom_x_shift
            chrom_height = self.__perbppix * chrom.length
            chrom_x_coords[chrom.name] = chrom_x_s
            self.__ctx.set_source_rgb(*color)
            self.__ctx.rectangle(chrom_x_s,chrom_y,self.__chromwidth,chrom_height)
            self.__ctx.fill()
            self.__ctx.set_source_rgb(0,0,0)
            self.__ctx.move_to(chrom_x_s, chrom_y - 10)
            self.__ctx.set_font_size(14)
            self.__ctx.show_text(chrom.desname)
        self.__chrom_x_coords = chrom_x_coords
        if(self.__centromere is not None):
            self.__draw_centromere()
    
    def __draw_centromere(self):
        chrom_y = hborder = self.__height * self.__border + self.__labelpix
        color = self.__format_color(self.__centrocolor)
        color.append(0.6) ##add translucent
        for region in self.__centromere:
            chrom = region.chrom
            reg_x = self.__chrom_x_coords[chrom] - self.__centrowidth
            reg_width = self.__chromwidth + 2 * self.__centrowidth
            reg_y = chrom_y + self.__perbppix * region.spos
            reg_height = self.__perbppix * region.reg_len()
            self.__ctx.set_source_rgba(*color)
            self.__ctx.rectangle(reg_x,reg_y,reg_width,reg_height)
            self.__ctx.fill()
            
    def __draw_regionsets(self):
        sets_num = len(self.__regionsets)
        
        isMulticolor = False
        regionColors = []
        regionColor = []
        regionwidth = (self.__chromspace - self.__regionchromspace - \
                       sets_num * self.__regionspace) / sets_num
        regionwidth = round(regionwidth,1)
        if(self.__autoregioncolor == False):
            if(len(self.__regioncolors) < 1):
                regionColor = self.__regioncolor
            else:
                if(len(self.__regioncolors) == sets_num):
                    regionColors = self.__regioncolors
                    isMulticolor = True
                else:
                    raise ValueError ("number of chrom colors is not equal to chrom number")
        else:
            regionColors = self.__color_generator(sets_num)
            isMulticolor = True
        
        chrom_y = hborder = self.__height * self.__border + self.__labelpix
        for i in range(sets_num):
            regions = self.__regionsets[i]
            color = []
            regions_x_shift = (i+1) * self.__regionspace + i * regionwidth
            if(isMulticolor):
                color = regionColors[i]
            else:
                color = regionColor
            color = self.__format_color(color)
            for reg in regions:
                chrom = reg.chrom
                chrom_x = self.__chrom_x_coords[chrom]
                reg_x = chrom_x + self.__chromwidth + regions_x_shift
                reg_y = chrom_y + self.__perbppix * reg.spos
                reg_height = self.__perbppix * reg.reg_len()
                self.__ctx.set_source_rgb(*color)
                self.__ctx.rectangle(reg_x,reg_y,regionwidth,reg_height)
                self.__ctx.fill()
    
    def draw_regions(self):
        if(self.__chroms == None):
            raise ValueError ("no genome infomation")
        self.__init_draw()
        self.__draw_ticks()
        self.__draw_chroms()
        self.__draw_regionsets()
        self.__end_draw()

class GenomeRegionSets(Sequence):
    """Class for store group of regions"""
    def __init__(self,genomeInfo):
        self.__genome = genomeInfo
        self.__regions = []

    def __len__(self):
        return len(self.__regions)

    def __getitem__(self, index):
        return self.__regions[index]

    def add_regionset(self, regionInfo):
        if(regionInfo.get_genome_info() != self.__genome):
            raise ValueError("Chromosome profiles do not match")
        else:
            self.__regions.append(regionInfo)

    def get_genome_info(self):
        return self.__genome

class GenomeRegions(Sequence):
    """class for storing all chromosome regions"""
    def __init__(self,gInfo):
        self.__genome = gInfo
        self.__region = []
        self.__region_name = ""

    def __getitem__(self, index):
        return self.__region[index]

    def __len__(self):
        return len(self.__region)

    def add_region(self, rInfo):
        """add region using RegionInfo Class"""
        regioninfo = rInfo.reg_Info()
        if(rInfo.spos >= rInfo.epos):
            raise ValueError ("start pos is larger than end pos in \
                              region: %s" %(regioninfo))
        if(self.__genome.check_chrom_region(rInfo)):
            self.__region.append(rInfo)
        else:
            raise ValueError ("region %s not in chromsome region" \
                              %(regioninfo))

    def add_region_info(self, cChrom, cStart, cEnd, cDir = "+", cDes = ""):
        """add region using seperated information"""
        region = RegionInfo(cChrom, cStart, cEnd, cDir, cDes)
        regioninfo = region.reg_info()
        if(region.spos >= region.epos):
            raise ValueError ("start pos is larger than end pos in \
                              region: %s" %(regioninfo))
        if(self.__genome.check_chrom_region(region)):
            self.__region.append(region)
        else:
            raise ValueError ("region %s not in chromsome region" \
                              %(regioninfo))
    
    def get_genome_info(self):
        return self.__genome
    
    def set_region_name(self,regionName):
        self.__region_name = regionName

class GenomeInfo(Sequence):
    """Genome class for storing all chromosomes information"""
    def __init__(self):
        self.chroms = []

    def __getitem__(self, index):
        return self.chroms[index]

    def __len__(self):
        return len(self.chroms)

    def __eq__(self, other):
        if(not isinstance(other, self.__class__)):
            return False
        if(len(self) != len(other)):
            return False
        for i in range(0, len(self)):
            if(self[i] != other[i]):
                return False
        return True

    def add_chrom(self, cInfo):
        """add new chromsome to genome"""
        self.chroms.append(cInfo)

    def add_chrom_info(self, cName, cLength, cDes = ""):
        """add new chromsome to genome"""
        chrom = ChromInfo(cName, cLength, cDes)
        self.chroms.append(chrom)

    def get_chrom_length(self, cName):
        """return length of specific chromsome"""
        for chrom in self.chroms:
            if(chrom.name == cName):
                return chrom.length
        raise ValueError ("Cannot find chromsomes named %s" %(cName))

    def get_chrom_names(self):
        """return a list contains all the chrom names"""
        cnames = []
        for chrom in self.chroms:
            cnames.append(chrom.name)
        return cnames

    def get_chrom_lengths(self):
        """return a list contains all the chrom length"""
        clength = []
        for chrom in self.chroms:
            clength.append(chrom.length)
        return clength

    def get_chrom_descs(self):
        """return a list contains all the chrom description"""
        cdesc = []
        for chrom in self.chroms:
            cdesc.append(chrom.desname)
        return cdesc

    def get_longest_chrom(self):
        """return the length of the longest chromsome"""
        maxlength = 0
        for chrom in self.chroms:
            if(chrom.length > maxlength):
                maxlength = chrom.length
        return maxlength

    def check_chrom_region(self, cRegion):
        """check chromsome region in genome"""
        blchrom = False
        for chrom in self.chroms:
            if(cRegion.chrom == chrom.name):
                blchrom = True
        if(not blchrom):
            return False

        chromlen = self.get_chrom_length(cRegion.chrom)
        if(cRegion.spos < 0 or cRegion.epos > chromlen):
            return False
        return True


class ChromInfo(object):
    """class for chromsome information storage"""
    def __init__(self, cName, cLength, cDes = ""):
        self.name = cName
        self.length = int(cLength)
        if(cDes == ""):
            self.desname = cName
        else:
            self.desname = cDes

    def __eq__(self, other):
        if(not isinstance(other, self.__class__)):
            return False
        if(self.name != other.name):
            return False
        if(self.length != other.length):
            return False
        return True

class RegionInfo(object):
    """class for genome region storage"""

    def __init__(self, cChrom, cStart, cEnd, cDir = "+", cDes = ""):
        self.chrom = cChrom
        self.spos = int(cStart)
        self.epos = int(cEnd)
        self.dir = cDir
        self.des = cDes

    def reg_len(self):
        """return length of the region"""
        reglen = self.epos - self.spos + 1
        return reglen

    def reg_info(self):
        infotxt = ("%s:%s-%s" %(self.chrom, self.spos, self.epos))
        return infotxt


def main():
    
    H99Genome = CNA2GenomeInfo()
    dupregion = GenomeRegions(H99Genome.chroms)
    dupregion.add_region_info("supercont2.1",123,2500)
    dupregion.add_region_info("supercont2.2",6800,8200)
    dupregion.add_region_info("supercont2.3",780000,840000)
    
    regionSets = GenomeRegionSets(H99Genome.chroms)
    regionSets.add_regionset(dupregion)
    regionSets.add_regionset(dupregion)
    regionSets.add_regionset(dupregion)
    regionSets.add_regionset(dupregion)
    vsGenome = VisualGenome()
    vsGenome.set_regions(regionSets)
    vsGenome.set_centromere(H99Genome.centromeres)
    vsGenome.set_auto_chrom_color(True)
    vsGenome.set_auto_region_color(True)
    vsGenome.set_output_file("/Users/Alvin/test.pdf")
    vsGenome.draw_regions()

if(__name__=="__main__"):
    main()
