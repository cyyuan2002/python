#!/usr/bin/env python
from io import StringIO
import xml.etree.cElementTree as ET
import re

SPACE = "    "

class PopSample:
    def __init__(self, name, size):
        self.data = []
        self.name = name
        self.size = size
    
    def addData(self, haptype, num, seq):
        self.data.append(self.sampledata(haptype,num,seq))
    
    def output(self):
        ind1 = SPACE
        ind2 = SPACE*2
        output = StringIO()
        output.write("%sSampleName = \"%s\"\n" %(ind1,self.name))
        output.write("%sSampleSize = %s\n" %(ind1,self.size))
        output.write("%sSampleData = {\n" %(ind1))
        for i in range(len(self.data)):
            output.write("%s%s\n" %(ind1, self.data[i].output()))
        output.write("%s}\n" %(ind1))
        outstring = output.getvalue()
        return outstring
        
    class sampledata:
        def __init__(self, haptype, num, seq):
            self.haptype = haptype
            self.num = num
            self.seq = seq
        def output(self):
            outhaptype = "%03.0f" %self.haptype
            outstring = "%s %s %s" %(outhaptype, self.num, self.seq)
            return outstring
    
class ArlSeqs:
    '''this class is used to generate Arlquine input file
    using the given sequences and population information'''
    pops = {}
    title = ""
    popnum = 0
    
    def __init__(self, title):
        self.title = title
        self.popSamples = {}
        self.hapseq = {}
        self.haptype = {}
        self.popName = []
    
    def addSeqs(self, seqs):
        #seq dict with sample name as key, seq as value
        hseqs = {}
        hapcount = 0
        for seqN in seqs:
            #print (">%s\n%s" %(seqN, seqs[seqN]))
            if seqs[seqN] not in hseqs:
                hseqs[seqs[seqN]] = hapcount
                self.hapseq[hapcount] = seqs[seqN]
                self.haptype[seqN] = hapcount
                hapcount += 1
            else:
                self.haptype[seqN] = hseqs[seqs[seqN]]
        #print ("---------")
        
    def addPops(self, pops):
        if len(self.haptype) < 1:
            raise ValueError ("No sequence has been added!")
        
        self.pops = pops    
        self.popnum = len(pops)
        self.popNames = sorted(self.pops.keys())
        
        for popName in pops:
            popsamples = pops[popName]
            pophaptypes = {}
            for sample in popsamples:
                if sample not in self.haptype:
                    raise ValueError ("Sample sequence does not exist!")
                
                if self.haptype[sample] not in pophaptypes:
                    pophaptypes[self.haptype[sample]] = 1
                else:
                    pophaptypes[self.haptype[sample]] += 1
            
            popSample = PopSample(popName,len(popsamples))
            for hap in pophaptypes:
                popSample.addData(hap,pophaptypes[hap],self.hapseq[hap])
            self.popSamples[popName] = popSample
    
    def output(self):
        if len(self.popSamples) < 1:
            raise ValueError ("No population has defined")
        
        ind1 = SPACE
        ind2 = SPACE*2
        ind3 = SPACE*3
        outstring = StringIO()
        outstring.write("[Profile]\n")
        outstring.write("%sTitle = \"%s\"\n" %(ind1,self.title))
        outstring.write("%sNbSamples = %s\n" %(ind1,self.popnum))
        outstring.write("%sDataType = DNA\n" %(ind1))
        outstring.write("%sGenotypicData = 0\n" %(ind1))
        outstring.write("%sLocusSeparator = NONE\n" %(ind1))
        outstring.write("%sMissingData = \"?\"\n" %(ind1))
        outstring.write("%sCompDistMatrix = 1\n" %(ind1))
        outstring.write("[data]\n%s[[Samples]]\n" %(ind1))
        for popName in self.popNames:
            outstring.write(self.popSamples[popName].output())
        
        outstring.write("%s[[Structure]]\n" %(ind1))
        outstring.write("%sStructureName=\"A group of %s populations\"\n" %(ind2, self.popnum))
        outstring.write("%sNbGroups = 1\n" %(ind2))
        outstring.write("%sGroup = {\n" %(ind2))
        for popName in self.popNames:
            outstring.write("%s\"%s\"\n" %(ind3, popName))
        outstring.write("%s}\n" %(ind2))
        return outstring.getvalue()


class ArlResParser:
    '''This class is used to parser Arlequine XML result and extract AMOVA FST result'''
    
    def __init__(self, resfile):
        self.pops = []
        self.pwfstpval = []
        self.pwfst = []
        self.amova = []
        self.wdamova = []
        self.wdfst = 0
        self.wdfstpval = 0
        self.locusamova = []
        self.locusfst = []
        self.fstpval = 0
        self.fst = 0
        self.isamova = 0
        self.ispwfst = 0
        self.islocusamova = 0
        self.iswdamova = 0
        self.resfile = resfile
        if resfile[-3:] == 'xml':
            self._parseXML(resfile)
        elif resfile[-3:] == 'htm':
            self._parseHTML(resfile)
        else:
            raise ValueError ("Unknown file format!")
        
    def getAmova(self):
        if self.isamova:
            return self.amova
        else:
            return None
    
    def getFSTPval(self):
        if self.isamova:
            return self.fstpval
        else:
            return None
    
    def getFST(self):
        if self.isamova:
            return self.fst
        else:
            return None
    
    def getWDAmova(self):
        if self.iswdamova:
            return self.wdamova
        else:
            return None
    
    def getWDFST(self):
        if self.iswdamova:
            return self.wdfst
        else:
            return None
    
    def getWDFSTPval(self):
        if self.iswdamova:
            return self.wdfstpval
        else:
            return None
    
    def getPWFST(self, pop1, pop2):
        if not self.ispwfst:
            return None
        if self.ispwfst:
            indpop1 = self._getPopIndex(pop1)
            indpop2 = self._getPopIndex(pop2)
            if indpop1 is not None and indpop2 is not None:
                return self.pwfst[indpop1][indpop2]
            else:
                return None
    
    def getPWFSTPval(self, pop1, pop2):
        if not self.ispwfst:
            return None    
        if self.ispwfst:
            indpop1 = self._getPopIndex(pop1)
            indpop2 = self._getPopIndex(pop2)
            if indpop1 is not None and indpop2 is not None:
                return self.pwfstpval[indpop1][indpop2]
            else:
                return None
    
    def _getPopIndex(self, pop):
        if pop in self.pops:
            return self.pops.index(pop)
        else:
            return None
    
    def _parseHTML(self,HTMLfile):
        fh = open(HTMLfile, 'rU')
        bl_record = False
        bl_amova = False
        bl_pwpop = False
        bl_FSTVal = False
        bl_FSTPVal = False
        bl_amovabylocus = False
        bl_weightedamova = False
        content = []
        for line in fh:
            stpline = line.strip()
            if stpline == "":
                continue
            if re.match(r"^<i>Weir, B. S., 1996.</i>", stpline):
                self.isamova = 1
                bl_record = True
                bl_amova = True
                continue
            if re.match(r"^List of labels for population samples used below:", stpline):
                bl_record = True
                bl_pwpop = True
                continue
            if re.match(r"^Population pairwise FSTs", stpline):
                self.ispwfst = 1
                bl_record = True
                bl_FSTVal = True
                continue
            if re.match(r"^FST P values", stpline):
                bl_record = True
                bl_FSTPVal = True
                continue
            if re.match(r"^AMOVA Results for polymorphic loci only:", stpline):
                self.islocusamova = 1
                bl_record = True
                bl_amovabylocus = True
                continue
            if re.match(r"^Global AMOVA results as a weighted average over loci", stpline):
                self.iswdamova = 1
                bl_record = True
                bl_weightedamova = True
                continue
            
            if re.match(r"^<A", stpline):
                if bl_amova:
                    bl_amova = False
                    bl_record = False
                    self._readAmova(content)
                    content = []
            if re.match(r"^--", stpline):
                if bl_record and len(content) > 3:
                    if bl_pwpop:
                        bl_pwpop = False
                        bl_record = False
                        self.pops = self._readPop(content)
                        content = []
                    if bl_FSTVal:
                        bl_FSTVal = False
                        bl_record = False
                        self.pwfst = self._readMatrix(content,self.pops)
                        content = []
                    if bl_FSTPVal:
                        bl_FSTPVal = False
                        bl_record = False
                        self.pwfstpval = self._readMatrix(content, self.pops)
                        content = []
            if re.match(r"^===", stpline):
                if bl_amovabylocus:
                    bl_amovabylocus = False
                    bl_record = False
                    self._readLocusAmova(content)
                    content = []
            if re.match(r"^///", stpline):
                if bl_weightedamova:
                    bl_weightedamova = False
                    bl_record = False
                    self._readWeightedAmova(content)
                    content = []
            if bl_record:
                content.append(stpline)
    
    def _parseXML(self,XMLfile):
        tree = ET.parse(XMLfile)
        root = tree.getroot()
        isAmova = 0
        lastcontent = ""
        for child in root:
            if child.tag == 'A':
                if isAmova:
                    content = self._rmEmptyLines(lastcontent)
                    #print '\n'.join(content)
                    self._readAmova(content)
                name = child.get('NAME')
                amova = re.findall("pop_amova$",name)
                if len(amova) > 0:
                    isAmova = 1
                    self.isamova = 1
            
            if isAmova:
                if child.tag == "data":
                    lastcontent = child.text

            ##get popinfo
            if child.tag == "pairDistPopLabels":
                content = self._rmEmptyLines(child.text)
                self.pops = self._readPop(content)
                #print '\n'.join(content)
                       
            ##get pairwise FST matrix
            if child.tag == "PairFstMat":
                self.ispwfst = 1
                content = self._rmEmptyLines(child.text)
                self.pwfst = self._readMatrix(content[1:], self.pops)
                #print '\n'.join(content)
            
            ##get pairwise FST pvalue matrix
            if child.tag == "PairFstPvalMat":
                content = self._rmEmptyLines(child.text)
                self.pwfstpval = self._readMatrix(content, self.pops)
                #print '\n'.join(content)
    
    def _rmEmptyLines(self, content):
        fltcontent = []
        for line in content.splitlines():
            line = line.strip()
            if line == '':
                continue
            fltcontent.append(line)
        return fltcontent
    
    def _readAmova(self, content):
        for line in content:
            if re.match("populations", line):
                info = line.split()
                self.amova.append(info[-1])
                continue
            m = re.match(r"P-value =  (\d+.\d+)",line)
            if m:
                self.fstpval = m.group(1)
                continue
            if re.match("Fixation Index", line): ##match first global FST
                info = line.split()
                self.fst = info[-1]
                continue
            if re.match("^FST :", line):
                info = line.split()
                self.fst = infop[-1]
                continue
    
    def _readPop(self, content):
        pops = []
        for line in content:
            if re.match(r"\d:", line):
                info = line.split()
                pops.append(info[-1])
            if re.match("</pairDistPopLabels>", line):
                popsec = 0
        return pops
    
    def _fltMatrixline(self, matrixlines):
        filtered = []
        for line in matrixlines:
            info = line.split()
            if not info[0].isdigit():
                continue
            filtered.append(line)
        return filtered
    
    def _readMatrix(self, matrixlines, pops):
        popnum = len(pops)
        ##create matrix with value init -1
        Matrix = [['*' for x in range(popnum)] for x in range(popnum)]
        pwvalue = {}
        matrixlines = self._fltMatrixline(matrixlines)
        for i in range(1,len(matrixlines)):
            info = matrixlines[i].split()
            for j in range(1, len(info)):
                if (i-1) == (j-1):
                    continue
                pval = re.match(r"([-+]?\d*\.\d+|\d+)", info[j])
                Matrix[i-1][j-1] = pval.group(0)
                Matrix[j-1][i-1] = pval.group(0)
        #print Matrix
        return Matrix
    
    def _readLocusAmova(self, content):
        locuscount = 0
        for line in content:
            cols = line.split()
            if not cols[0].isdigit():
                continue
            locuscount += 1
            var = float(cols[3])
            fst = float(cols[9])
            self.locusamova.append(var)
            self.locusfst.append(var)
            
    def _readWeightedAmova(self, content):
        for line in content:
            if re.match("populations", line):
                info = line.split()
                self.wdamova.append(info[-1])
                continue
            m = re.match(r"P-value =  (\d+.\d+)",line)
            if m:
                self.wdfstpval = m.group(1)
                continue
            if re.match("Fixation Index", line): ##match first global FST
                info = line.split()
                self.wdfst = info[-1]
                continue
            if re.match("^FST :", line):
                info = line.split()
                self.wdfst = info[-1]
                continue
    
if __name__ == '__main__':
    seqs = {}
    seqs['a'] = "GACTCTCTACGTAGCATCCGATGACGATA"
    seqs['b'] = "GACTCTCTACGTAGCATCCGATGACGATA"
    seqs['c'] = "GACTCTCTACGTAGCATCCGATGACGATA"
    seqs['d'] = "GACTGTCTGCGTAGCATACGACGACGATA"
    seqs['e'] = "GCCTGTCTGCGTAGCATAGGATGACGATA"
    seqs['f'] = "GCCTGTCTGCGTAGCATAGGATGACGATA"
    
    seqs['g'] = "GACTCTCTACGTAGCATCCGATGACGATA"
    seqs['h'] = "GACTCTCTACGTAGCATCCGATGACGATA"
    
    pops = {"Pop1":['a','b','c','d'], "Pop2":['e','f','g','h']}
    arlseq = ArlSeqs("An example of DNA sequence data")
    arlseq.addSeqs(seqs)
    arlseq.addPops(pops)
    print((arlseq.output()))
    
    xmlfile = "/Users/Alvin/Dropbox/TempSave/Alertest/test2.res/test2.htm"
    arlXML  = ArlResParser(xmlfile)
    amova = arlXML.getAmova()
    amovapval = arlXML.getFSTPval()
    fst = arlXML.getFST()
    pwfst = arlXML.getPWFST('Pop1','Pop2')
    pwfstpval = arlXML.getPWFSTPval('Pop1','Pop2')
    
    