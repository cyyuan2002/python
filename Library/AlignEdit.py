#!/usr/bin/env python
import sys
import numpy as np
from numpy import transpose
from Bio.Align import MultipleSeqAlignment as MultiAlign
from Bio.Alphabet import IUPAC, Gapped
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from os.path import isfile
from Bio import AlignIO

def ReadAlignFile(FileName, Format):
    """Read alignment from given file with the format provided"""
    
    if(not isfile(FileName)):
        raise IOError ("ReadAlignFile: cannot open file %s\n\n" %(FileName))
    
    fh_file = open (FileName,'rU')
    alignment = AlignIO.read(fh_file, Format, alphabet=Gapped(IUPAC.ambiguous_dna))
    fh_file.close()
    return alignment

def TrimAlignment(Alignment, TrimMode, TrimA, TrimB=0):
    """Trim the end of alignments in two modes,
    Mode 1: trim the same length from both end;
    Mode 2: trim different lengths from each end.
    """

    trimmed = MultiAlign(None,alphabet=Gapped(IUPAC.ambiguous_dna))
    for seq in Alignment:
        seqlen = len(seq)
        if TrimMode == 1:
            if(TrimA > 0):
                seqend = seqlen - TrimA
                seq = seq[TrimA:seqend]
        else:
            if TrimB == 0:
                seq = seq[(TrimA-1):]
            else:
                TrimB = TrimB - 1
                seq = seq[(TrimA-1):TrimB]
        trimmed.append(seq)
    return trimmed

def ConcAlign(Align1,Align2,Mode=0):
    """Concatenate two align sequences together
    Mode=0 do not allow missing sequences, Mode=1 allows missing sequences"""
    
    ConcAlign = MultiAlign(None,alphabet=Gapped(IUPAC.ambiguous_dna))
    
    alignNum1 = len(Align1)
    alignNum2 = len(Align2)
    alignLen1 = Align1.get_alignment_length()
    alignLen2 = Align2.get_alignment_length()
    alignName1 = GetSeqNames(Align1)
    alignName2 = GetSeqNames(Align2)
    alignNames = set.union(alignName1,alignName2)
    
    if(Mode == 1):
        if(alignNum1 != alignNum2):
            raise ValueError("ConcAlign: different sequence number between Align1 and Align2")
    
    for seqN in alignNames:
        seq1 = GetSeq(seqN, Align1)
        seq2 = GetSeq(seqN, Align2)
        if(seq1 is None):
            seq1 = '-'*alignLen1
        if(seq2 is None):
            seq2 = '-'*alignLen2
        concseq = str(seq1 + seq2)
        seqrec = SeqRecord(Seq(concseq,Gapped(IUPAC.ambiguous_dna)),
                           id=seqN, name = "",description = "")
        ConcAlign.append(seqrec)
    
    return ConcAlign

def OutputAlignment(OutFile, Format):
    """Output the alignment file to the given file name according to the format"""

    fh_outfile = open(OutFile,'w')
    AlignIO.write(AlignSeq,fh_outfile,outformat)
    fh_outfile.close()
    return (1)

def RemoveAllGaps(Alignment):
    """Remove all the gap region in the alignment sequences"""

    poloci = []
    newalign = None
    align_array = np.array([list(rec) for rec in Alignment], np.character)
    npshape = align_array.shape
    
    for i in range(npshape[0]):
        isgap = False
        for j in range(npshape[1]):
            if(align_array[i][j] == '-'):
                isgap = True
                break
        if(not isgap):
            if(newalign is None):
                newalign = Alignment[:,i]
            else:
                newalign += Alignment[:,i]
    
    return newalign

def GetSeq(SeqId, Alignment, mode=0):
    """return Seq for the providing SeqId, mode 0: return str; mode 1: return seq"""
    for seq in Alignment:
        if(seq.id == SeqId):
            if(mode == 1):
                return seq
            else:
                return seq.seq
    return None

def GetSeqNames(Alignment):
    """return sequence names in the alignment"""
    seqnames = []
    for seq in Alignment:
        if(seq.id in seqnames):
            raise ValueError("GetSeqNames: duplicated sequence name %d in alignment" %(seq.id))
        seqnames.append(seq.id)
    return set(seqnames)

def SortAlign(Alignment, SeqOrder):
    """Select and sort the alignments according to the given order"""
    sortedAlign = MultiAlign(None,alphabet=Gapped(IUPAC.ambiguous_dna))
    for seqN in SeqOrder:
        seq = GetSeq(seqN, Alignment, 1)
        if(seq is None):
            raise ValueError("ReorderAlign: cannot find sequence %s in alignment" %(seqN))
        sortedAlign.append(seq)
    return sortedAlign

def SeqDiversity(Seq1,Seq2):
    """Caculate nucleotide diversity between two aligned sequences"""
    if(len(Seq1) != len(Seq2)):
        raise ValueError("SeqDiversity: sequence lengths are unequal!\n")
    
    distscore = 0
    for i in range(len(Seq1)):
        if(Seq1[i] != Seq2[i]):
            distscore += 1
    
    return distscore

def SearchVariance(Aligns): ##search for variant in the sequences
    """Search variant nuclotide in the alignment"""
    
    Nucletides = ["A","T","C","G","N","-"]

    alignlen = Aligns.get_alignment_length()
    allvariant = []
    variantsites = []
    for n in range(alignlen):
        base_dict = {}
        for record in Aligns:
            curbase = record.seq[n]
            upbase = curbase.upper()
            if(upbase not in Nucletides):
                upbase = 'N'  #Other bases covert to 'N'
            if(upbase not in base_dict):
                base_dict[upbase] = 1
            else:
                base_dict[upbase] += 1

        if(len(base_dict) > 1):
            if(len(base_dict) == 2 and 'N' in base_dict): # remove no informative site
                continue
            else:
                bases = []
                for record in Aligns:
                    curbase = record.seq[n]
                    upbase = curbase.upper()
                    bases.append(upbase)
                allvariant.append(bases)
                variantsites.append(n)

    transvars = list(map(list,list(zip(*allvariant))))
    rowcount = 0
    variantalign = MultiAlign(None,alphabet=Gapped(IUPAC.ambiguous_dna))
    for record in Aligns:
        seqid = record.id
        seq = "".join(transvars[rowcount])
        newseq = SeqRecord(Seq(seq,Gapped(IUPAC.ambiguous_dna)),
                           id=seqid, name = "",description = "")
        variantalign.append(newseq)
        rowcount += 1
                
    return (variantalign, variantsites)


class Align2Number:
    """class to convert SeqAlign to NumAlign"""
    class _SeqNumber:
        def __init__(self, ID, Seq):
            self.id = ID
            self.seq = Seq
    
    def __init__(self, Aligns, Dict):
        self.orialign = Aligns
        self.dict = Dict
        
        self._align2number()
        
    def __getitem__(self, index):
        return self.align[index]
    
    def __len__(self):
        return len(self.align)
    
    def _align2number(self):
        TransAlign = []
        for record in self.orialign:
            numbase = self._bases2number(record.seq, self.dict)
            numseq = self._SeqNumber(record.id, numbase)
            TransAlign.append(numseq)
        self.align = TransAlign
    
    def _bases2number(self, Bases, Dict):
        transbase = []
        for n in range(len(Bases)):
            if(Bases[n] not in Dict):
                raise ValueError("Align2Number: %s is not in the dictionary" %(Bases[n]))
            else:
                num = Dict[Bases[n]]
                transbase.append(str(num))
        return transbase        
