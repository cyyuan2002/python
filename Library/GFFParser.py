#!/usr/bin/env python
import re
import operator
import sys
import csv
import copy

class DNA_Region(object):
    """smallest unit in gff3 annotation"""
    def __init__(self,start,end,dr):
        start = int(start)
        end = int(end)
        if(start > end):
            tmp = start
            start = end
            end = tmp
        self.s = start
        self.e = end
        self.d = dr
        self.len = self.e - self.s + 1
    def __len__(self):
        return(self.len)
    def getDir(self):
        return(self.d)
    def inRegion(self, pos):
        if(int(pos) >= start and int(pos) <= end):
            return(1)
        else:
            return(0)

class RNA_Region(object):
    def __init__(self):
        self.id = ""
        self.parent = ""
        self.alias = ""
        self.s = 0
        self.e = 0
        self.d = '+'
        self.len = 0
        self._isupdated = False
        self.p5UTR = []
        self.p3UTR = []
        self.CDSs = []
        self.exons = []
        self.introns = []
    
    def __len__(self):
        if(not self._isupdated):
            self.updateRNA()
        return (self.len)
    
    def setRgn(self, start, end, dr):
        start = int(start)
        end = int(end)
        if(start <= end):
            self.s = start
            self.e = end
        else:
            self.s = end
            self.e = start
        self.d = dr
    
    def addProp(self, info): #sample: ID=7000009599297531;Alias=CNAG_05668T0;Parent=CNAG_05668;Name=hypothetical protein
        infos = info.split(';')
        for i in range(len(infos)):
            cont = infos[i].split('=')
            if(cont[0] == "ID"):
                self.id = cont[1]
            elif(cont[0] == "Alias"):
                self.alias = cont[1]
            elif(cont[0] == "Parent"):
                self.parent = cont[1]
            elif(cont[0] == "Name"):
                self.name = cont[1]
    
    def addExon(self, start, end):
        self._isupdated = False
        self.exons.append(DNA_Region(start, end, self.d))

    def addCDS(self, start, end):
        self._isupdated = False
        self.CDSs.append(DNA_Region(start, end, self.d))
    
    def updateRNA(self):
        self.exons = self._sortRgns(self.exons)
        self.CDSs = self._sortRgns(self.CDSs)
        self._defineUTRs()
        self._defineIntrons()
        self.len = self._rgnLength(self.exons)
        self._isupdated = True
    
    def getIntronLength(self):
        if(not self._isupdated):
            self.update()
        length = self._rgnLength(self.introns)
        return (length)
    
    def get5UTRLength(self):
        if(not self._isupdated):
            self.update()
        length = self._rgnLength(self.p5UTR)
        return (length)
    
    def get3UTRLength(self):
        if(not self._isupdated):
            self.update()
        length = self._rgnLength(self.p3UTR)
        return (length)
    
    def getCDSLength(self):
        if(not self._isupdated):
            self.update()
        length = self._rgnLength(self.CDSs)
        return (length)
    
    def getRNALength(self):
        if(not self._isupdated):
            self.update()
        return (self.len)
    
    def checkSite(self, site):
        site = int(site)
        if site < self.s or site > self.e:
            return None
        if self._checkSiteRgn(site, self.p5UTR):
            return "5UTR"
        if self._checkSiteRgn(site, self.p3UTR):
            return "3UTR"
        if self._checkSiteRgn(site, self.CDSs):
            return "CDS"
        return "Intron"
        
    def _checkSiteRgn(self, site, regions):
        for rgn in regions:
            if site >= rgn.s and site <= rgn.e:
                return True
        return False
    
    def _sortRgns(self, rgns):
        sortedrgns = sorted(rgns, key=lambda x: x.s)
        return sortedrgns
    
    def _defineUTRs(self):
        self.p5UTR = []
        self.p3UTR = []
        UTR_s = self.CDSs[0].s - 1
        UTR_e = self.CDSs[-1].e + 1
        UTR_A = []
        UTR_B = []
        for i in range(len(self.exons)):
            if(self.exons[i].s >= UTR_s):
                break
            elif(self.exons[i].s < UTR_s):
                if(self.exons[i].e < UTR_s):
                    UTR_A.append(self.exons[i])
                else:
                    rgn = DNA_Region(self.exons[i].s,UTR_s,self.exons[i].d)
                    UTR_A.append(rgn)
                    break
        
        for i in range(len(self.exons)):
            if(self.exons[i].s <= UTR_e and self.exons[i].e >= UTR_e):
                rgn = DNA_Region(UTR_e,self.exons[i].e,self.exons[i].d)
                UTR_B.append(rgn)
            elif(self.exons[i].s > UTR_e):
                UTR_B.append(self.exons[i])
        
        if(self.d == '+'):
            self.p5UTR = UTR_A
            self.p3UTR = UTR_B
        else:
            self.p5UTR = UTR_B
            self.p3UTR = UTR_A
    
    def _defineIntrons(self):
        self.introns = []
        start = 0
        end = 0
        if(len(self.exons) == 1):
            return 
        for i in range(len(self.exons)):
            if(start != 0):
                end = self.exons[i].s - 1
                rgn = DNA_Region(start,end,self.d)
                self.introns.append(rgn)
            start = self.exons[i].e + 1
        if(self.d == '-'):
            self.introns.reverse()
            
    def _rgnLength(self, rgns):
        totallen = 0
        for i in range(len(rgns)):
            totallen += len(rgns[i])
        return totallen

class Gene_Region(object):
    def __init__(self, chrom, start, end, dr):
        self.mRNA = []
        start = int(start)
        end = int(end)
        if(start <= end):
            self.s = start
            self.e = end
        else:
            self.s = end
            self.e = start
        self.len = self.e - self.s + 1
        
        self.id = ""
        self.alias = ""
        self.name = ""
        self.chrom = chrom
        self.d = dr
    
    def addProp(self,info):
        infos = info.split(';')
        for i in range(len(infos)):
            cont = infos[i].split('=')
            if(cont[0] == "ID"):
                self.id = cont[1]
            elif(cont[0] == "Alias"):
                self.alias = cont[1]
            elif(cont[0] == "Name"):
                self.name = cont[1]
    
    def addRNA(self, rna_region):
        self.mRNA.append(rna_region)
    
    def numofRNA(self):
        return (len(self.mRNA))
    
    def longestRNA(self):
        length = self.mRNA[0].getRNALength()
        index = 0
        for i in range(1,len(self.mRNA)):
            if self.mRNA[i].getRNALength() > length:
                length = self.mRNA[i].getRNALength()
                index = i
        return self.mRNA[index]
    
    def checkSite(self, site):
        site = int(site)
        if site < self.s or site > self.e:
            return None
        else:
            for i in range(len(self.mRNA)):
                sitetype = self.mRNA[i].checkSite(site)
                if sitetype:
                    return sitetype
            raise ValueError ("Undefined site on genome")
    
    
    def __len__(self):
        return (self.len)


class Chrom_Region(object):
    def __init__(self, chromID):
        self.id = chromID
        self.gids = []
        self.genes = {}
        self.geneIndex = {}
        self.sorted = False
        
    def addGene(self, gene_region):
        self.genes[gene_region.id] = gene_region
        self.sorted = False
    
    def _sortGenes(self):
        self.gids = sorted(self.genes, key=lambda x: self.genes[x].s)
        self.sorted = True
    
    def numofGenes(self):
        return (len(self.genes))
    
    def getGeneIds(self): ##return all gds in chrom
        if not self.sorted:
            self.gids = sorted(self.genes, key=lambda x: self.genes[x].s)
        return self.gids
    
    
    def getGene(self, geneID): ##get gene info by gid
        if(geneID in self.genes):
            return (self.genes[geneID])
        else:
            return (False)

    def __len__(self):
        return (len(self.genes))
    
    def __getitem__(self,gid):
        if not self.sorted:
            self._sortGenes()
        return (self.genes[gid])
    
    def __iter__(self):
        if not self.sorted:
            self._sortGenes()
        for gid in self.gids:
            yield gid
    
class GFFParser(object):
    def __init__(self, filename = ""):
        self.chroms = []
        self.gffinfo = {}
        self.gfffile = filename
        self.genes = {}
        if(self.gfffile != ""):
            self.open(self.gfffile)
    
    def open(self, gfffile):
        try:
            self.gfffile = gfffile
            f = open(gfffile, 'rU')
            currgene = None
            currmRNA = None
            lastchrom = ""
            for readline in f:
                stripedline = readline.strip()
                line = stripedline.split('\t')
                if(len(line) < 2):
                    continue
                if(line[2] == 'gene'):
                    if(currgene):
                        currmRNA.updateRNA()
                        currgene.addRNA(copy.deepcopy(currmRNA))
                        self.gffinfo[lastchrom].addGene(copy.deepcopy(currgene))
                    lastchrom = line[0]
                    if(line[0] not in self.chroms):
                        self.chroms.append(line[0])
                        self.gffinfo[line[0]] = Chrom_Region(line[0])
                    currgene = Gene_Region(line[0], line[3], line[4], line[6])
                    currgene.addProp(line[8])
                    currmRNA = None
                elif(line[2] == 'mRNA'):
                    if(currmRNA):
                        currmRNA.updateRNA()
                        currgene.addRNA(copy.deepcopy(currmRNA))
                    currmRNA = RNA_Region()
                    currmRNA.setRgn(line[3], line[4], line[6])
                    currmRNA.addProp(line[8])
                elif(line[2] == 'exon'):
                    currmRNA.addExon(line[3], line[4])
                elif(line[2] == 'CDS'):
                    currmRNA.addCDS(line[3], line[4])
            currmRNA.updateRNA()
            currgene.addRNA(copy.deepcopy(currmRNA))
            self.gffinfo[lastchrom].addGene(copy.deepcopy(currgene))
            f.close()
            self._getallgenes()

        except IOError as e:
            print(("I/O error({0}): {1}".format(e.errno, e.strerror)))
            sys.exit(1)
    
    ##return the site location on the genome
    def siteType(self, chrom, site):
        if chrom not in self.chroms:
            return ("intergenic", None)
        gids = self.gffinfo[chrom].getGeneIds()
        site = int(site)
        for gid in gids:
            #print ("%s\t%s\t%s" %(gid, self.genes[gid].s, self.genes[gid].e))
            if site < self.genes[gid].s:
                return ("intergenic", None)
            if site >= self.genes[gid].s and site<= self.genes[gid].e:
                sitetype = self.genes[gid].checkSite(site)
                return (sitetype, gid)
        return ("intergenic", None)
    
    def getChroms(self):
        self._sortchrom()
        return (self.chroms)
    
    def numofChroms(self):
        return (len(self.chroms))
    
    def _sortchrom(self):
        self.chroms = sorted(self.chroms, key=str.lower)
    
    def numofGenes(self):
        genenum = 0
        for chrom in self.gffinfo:
            genenum += self.gffinfo[chrom].numofGenes()
        return (genenum)
    
    def getGene(self, geneID): ##return gene information by givin gid
        if geneID in self.genes:
            return (self.genes[geneID])
        return (False)
    
    def _getallgenes(self):
        for chrom in self.gffinfo:
            chromgff = self.gffinfo[chrom]
            genes = chromgff.getGeneIds()
            for gid in genes:
                self.genes[gid] = chromgff.genes[gid]
    
    def __getitem__(self, chrom):
        return (self.gffinfo[chrom])
    
    def __iter__(self):
        self._sortchrom()
        for chrom in self.chroms:
            yield chrom

if __name__ == '__main__':
    filen = sys.argv[1]
    gffparser = GFFParser()
    gffparser.open(filen)
    lcoding = 0
    lgene = 0 
    l5UTR = 0
    l3UTR = 0
    lintron = 0
    
    #for chrom in gffparser:
    #    chromgff = gffparser[chrom]
    #    for geneindex in chromgff:
    #        gene = chromgff[geneindex]
    #        lgene += len(gene)
    #        mRNA = gene.mRNA[0]
    #        lcoding += mRNA.getCDSLength()
    #        l5UTR += mRNA.get5UTRLength()
    #        l3UTR += mRNA.get3UTRLength()
    #        lintron += mRNA.getIntronLength()
    sitetype, gene = gffparser.siteType("supercont2.1", 2288498)
    print((sitetype + gene))
    
    print(("Coding Length: %s" %(lcoding)))
    print(("Gene Length: %s" %(lgene)))
    print(("5UTR Length: %s" %(l5UTR)))
    print(("3UTR Length: %s" %(l3UTR)))
    print(("Intron Length: %s" %(lintron)))