#!/usr/bin/env python

import csv
import sys

def ReadVCFSites(filename):
    sites = {}
    with open(filename, 'rU') as fh:
        reader = csv.reader(fh, delimiter="\t")
        for line in reader:
            if line[0][0] == "#":
                continue
            if line[0] not in sites:
                tmp = []
                sites[line[0]] = tmp
            sites[line[0]].append(line[1])
    return sites

def FilterVCFSites(vcffile, vcfsites):
    with open(vcffile, 'rU') as fh:
        reader = csv.reader(fh, delimiter="\t")
        snps = []
        chrom = ""
        for line in reader:
            if line[0][0] == "#":
                continue
            if line[0] != chrom:
                chrom = line[0]
                if(chrom not in vcfsites):
                    snps = []
                else:
                    snps = vcfsites[chrom]
            if line[1] in snps:
                print("\t".join(line))

if __name__ == '__main__':
    if len(sys.argv) < 3:
        sys.stderr.write ("Usage:%s <VCF_File> <VCF_Sites>\n" %(sys.argv[0]))
        sys.exit(1)
    
    VCFSites = ReadVCFSites(sys.argv[2])
    FilterVCFSites(sys.argv[1], VCFSites)

