
from .version import __version__

# make the module importable
from goatools.go_enrichment import *
from . import multiple_testing
from . import obo_parser
