#!/usr/bin/env python
#this script is sorting SNPs in genes based on the annotation file

import csv
import sys
import re

class GeneSNPStats:
    geneID = ""
    NON = 0
    SYN = 0
    NSY = 0
    p3UTR = 0
    p5UTR = 0
    
    def __init__(self, gid):
        self.geneID = gid
    
    def addSNP(self, num, snptype):
        if snptype == 'NON':
            self.NON += num
        elif snptype == 'SYN':
            self.SYN += num
        elif snptype == 'NSY':
            self.NSY += num
        elif snptype == 'p3UTR':
            self.p3UTR += num
        elif snptype == 'p5UTR':
            self.p5UTR += num
        else:
            raise ValueError ("Incorrect SNP type")
    
    def statOut(self):
        print(("%s\t%s\t%s\t%s\t%s\t%s" %(
            self.geneID, self.NON, self.NSY, self.SYN, self.p5UTR, self.p3UTR)))

if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.stderr.write ("Usage:%s <Annotated_File>")
        sys.exit(1)
        
    fh_antfile = open(sys.argv[1], 'rU')
    reader = csv.reader(fh_antfile, delimiter = '\t')
    
    snpGenes = {}
    
    for line in reader:
        if line[0][0] == '#': #skip header line
            continue
        
        if len(line) < 1:
            continue
        
        anno = line[-1]
        splanno = anno.split(',')
        
        if len(splanno) < 2: ##remove intergenic region
            continue
        
        if splanno[0] == "intron":
            continue

        gid = splanno[1]
        if gid not in snpGenes:
            newgene = GeneSNPStats(gid)
            snpGenes[gid] = newgene
        
        if splanno[0] == 'p3UTR' or splanno[0] == 'p5UTR':
            snpGenes[gid].addSNP(1,splanno[0])
        else:
            snptype = re.match(r'\((\w+)\)', splanno[-1])
            snpGenes[gid].addSNP(1,snptype.group(1))

    fh_antfile.close()
    
    print ("#geneid\tNON\tNSY\tSYN\tp5UTR\tp3UTR")
    for gene in snpGenes:
        snpGenes[gene].statOut()

    