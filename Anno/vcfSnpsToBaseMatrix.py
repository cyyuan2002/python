#!/usr/bin/env python




import sys, re
import argparse
import vcfTools

parser = argparse.ArgumentParser()
parser.add_argument('infile', help='file of VCF filenames', type=str)
parser.add_argument('--max_amb_samples', help='maximum number of samples with ambiguous calls for a site to be included', type=int)
args = parser.parse_args()

infile = args.infile

max_amb = 1000000
if args.max_amb_samples:
	max_amb = args.max_amb_samples

comment_pattern = re.compile(r"^#")

ref_bases = {}
alt_bases = {}
passed_snp_positions = {}
genome_list = []
amb_pos_counts = {}

with open(infile, 'r') as fof:
	for fof_line in fof:
		sys.stderr.write("Searching " + fof_line)
		fof_line = fof_line.rstrip()
		header = vcfTools.VcfHeader(fof_line)
		caller = header.get_caller()
		samples = header.get_samples()
		per_file_genome_list = []
		for sample in samples:
			genome_list.append(sample)
			per_file_genome_list.append(sample)
						
		with open(fof_line, 'r') as vcf_file:
			for vcf_line in vcf_file:
				if not (re.search(comment_pattern, vcf_line)):
					record = vcfTools.VcfRecord(vcf_line)
					pass_or_fail = record.is_passing(caller)
					for genome in per_file_genome_list:
						genotype = record.get_genotype(index=header.get_sample_index(genome),min_gq=0)
						variant_type = record.get_variant_type(caller,genotype)
						### print(genome + " " + str(genotype) + " " + str(pass_or_fail) + " " + str(variant_type)) ###
						if pass_or_fail and not variant_type:
							pass
						elif pass_or_fail and variant_type == 'SNP':
							chrom = record.get_chrom()
							pos = int(record.get_pos())
					
							if not genome in alt_bases:
								alt_bases[genome] = {}
							if not chrom in alt_bases[genome]:
								alt_bases[genome][chrom] = {}
							alt_bases[genome][chrom][pos] = record.get_alt(genotype)
							### print(alt_bases[genome][chrom][pos]) ###
							### print(alt_bases) ###
					
							if not chrom in ref_bases:
								ref_bases[chrom] = {}						
							ref_bases[chrom][pos] = record.get_ref()
					
							if not chrom in passed_snp_positions:
								passed_snp_positions[chrom] = {}
							passed_snp_positions[chrom][pos] = True
						else:
							chrom = record.get_chrom()
							pos = int(record.get_pos())
							if not genome in alt_bases:
								alt_bases[genome] = {} 
							if not chrom in alt_bases[genome]: 
								alt_bases[genome][chrom] = {}							
							alt_bases[genome][chrom][pos] = 'N'
							if not chrom in amb_pos_counts:
								amb_pos_counts[chrom] = {}
							if not pos in amb_pos_counts[chrom]:
								amb_pos_counts[chrom][pos] = 1
							else:
								amb_pos_counts[chrom][pos] += 1
							### print(alt_bases[genome][chrom][pos]) ###
							### print(alt_bases) ###

### print(alt_bases) ###						

sorted_chroms = sorted(passed_snp_positions.keys())

print("Chrom\tPos\t", end="")
print("\t".join(genome_list))

for sorted_chrom in sorted_chroms:
	sorted_positions = sorted(passed_snp_positions[sorted_chrom])
	for sorted_position in sorted_positions:
		keep_flag = True
		if sorted_position in amb_pos_counts[sorted_chrom]:
			if amb_pos_counts[sorted_chrom][sorted_position] > max_amb:
				keep_flag = False
		if keep_flag:
			print("\t".join([str(sorted_chrom),str(sorted_position)]), end = "")
			for ind_genome in genome_list:
				if sorted_position in amb_pos_counts[sorted_chrom]:
					if amb_pos_counts[sorted_chrom][sorted_position] <= max_amb:
						if sorted_position in alt_bases[ind_genome][sorted_chrom]:
							print("".join(["\t",alt_bases[ind_genome][sorted_chrom][sorted_position]]), end = "")
						else:
							print("".join(["\t",ref_bases[sorted_chrom][sorted_position]]), end = "")
				else:
					if sorted_position in alt_bases[ind_genome][sorted_chrom]:
						print("".join(["\t",alt_bases[ind_genome][sorted_chrom][sorted_position]]), end = "")
					else:
						print("".join(["\t",ref_bases[sorted_chrom][sorted_position]]), end = "")
			print("")

