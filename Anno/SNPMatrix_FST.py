#!/usr/bin/env python
#This script is used to calculate FST using SNPMatrix and given pop information
#PopInfo format
## @pop1
## sample1
## sample2
## sample2
## @pop2
## sample3
## sample4

class fstlocus:
    def __init__(self, matrixline, samplecols):
        popnum = len(samplecols)
        alfreq = _getalfreq(matrixline,samplecols)
    
    def _getFST(matrixline, samplecols):
        popnum = len(samplecols)
        popnames = list(samplecols.keys())
        popHs = {}
        Fst = {}
        for pop in samplecols:
            popHs[pop] = _getH(matrixline, samplecols[pop])
        
        for i in range(popnum):
            for j in range(i+1,popnum):
                mergedcols = samplecols[popnames[i]] + samplecols[popnames[j]]
                Ht = _getH(matrixline, mergedcols)
                Hsbar = (popHs[popnames[i]] + popHs[popnames[j]])/2
                Fst = (Ht - Hsbar) / Ht
                Fstname = popnames[i] + ':' + popnames[j]
                Fst[Fstname] = Fst
                
        Hsbar = 0
        allcols = []
        for pop in samplecols:
            allcols += samplecols
            Hsbar += popHs[pop]
        Hsbar = Hsbar/popnum
        Htall = _getH(matrixline, allcols)
        Fst['all'] = (Htall - Hsbar) / Htall
        return Fst
        
    def _getH(matrixline,popcols):
        samplenum = len(popcols)
        alleles = {}
        for col in popcols:
            if alleles[matrixline[col]] not in alleles:
                alleles[matrixline[col]] = 1
            else:
                alleles[matrixline[col]] += 1
        
        if '.' in alleles:
            samplenum -= alleles['.']
        
        pi2 = 0
        for allele in alleles:
            if allele == '.':
                continue
            pi2 += (alleles[allele]/samplenum) ** 2
        Hs = 1 - pi2
        return Hs

def readPopInfo(popfile):
    fh_pop = open(popfile, 'rU')
    pops = {}
    popname = ''
    popitems = []
    for line in fh_pop:
        lineinfo = line.strip()
        if lineinfo[0] == '@':
            if popname != '':
                pops[popname] = copy.copy(popitems)
            popname = lineinfo[1:]
            popitems = ()
        else:
            popitems.append(lineinfo)
    fh_pop.close()
    pops[popname] = copy.copy(popitems)
    return (pops)

def readMatrix(mtxfile,pops):
    fh_matrix = open(mtxfile, 'rU')
    reader = csv.reader(fh_matrix, delimiter="\t")
    header = next(reader)
    samplecols = findCols(header, pops)

def findCols (header, pops):
    samplecols = {}
    for pop in pops:
        subpop = pops[pop]
        cols = []
        for sample in subpop:
            if sample not in header:
                sys.stderr.write("Error:cannot find %s in header!\n" %(sample))
                sys.exit(1)
            else:
                cols.append(header.index(strain))
        samplecols[pop] = cols
    return samplecols

import csv
import sys
import copy

if __name__ == '__main__':
    
    if len(sys.argv) < 3:
        sys.stderr.write ("Usage:%s <SNPMatrix> <PopInfo>")
        sys.exit(1)
    
    fmatrix = sys.argv[1]
    fpop = sys.argv[2]
    
    pops = readPopInfo(fpop)
    
   
