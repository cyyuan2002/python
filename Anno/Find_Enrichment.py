#!/usr/bin/env python

import scipy.stats as stats
import numpy as np
import pandas
import csv
import sys

class EnrichmentTest:
    '''This class is used to do chi-square test for enrichment
    analysis such as KEGG or PFAM. The 'geneAnno' should be dict with
    gene identifier as key and category info as value. Multiple values
    are seperated with ','. The 'termDesc' contains the description each categroy.
    The 'sampleList' is a list with all sample gene identifiers. If no full gene list
    provided, it will use all the genes in the infoList as default.
    '''
    
    def __init__(self, geneAnno, termDesc, selectedSamples, totalSamples = []):
        self.TermDesc = termDesc
        self.SelectedSamples = selectedSamples
        self.GeneAnno = geneAnno
        if (len(totalSamples) < 1):
            self.TotalSamples = list(geneAnno.keys())
        else:
            self.TotalSamples = totalSamples
        self.NumSamples = len(selectedSamples)
        self.NumTotal = len(totalSamples)
        self.isTested = False
        self.isAdjusted = False
        self.Pvalues = {}
        self.Qvalues = {}
        self.SelectedCate = {}
        self.TotalCate = {}
        self._checkData()
        samplecounts, sampleids = self._countCategory(self.SelectedSamples)
        self.SelectedCounts = samplecounts
        self.SelectedIds = sampleids
        
        totalcounts, totalids = self._countCategory(self.TotalSamples)
        self.TotalCounts = totalcounts
        self.TotalIds = totalids
    
    def runTest(self):
        samplecounts = self.SelectedCounts
        totalcounts = self.TotalCounts
        sampleids = self.SelectedIds
        totalids = self.TotalIds
        
        for catename in samplecounts:
            if (catename in totalcounts):
                #l = len(totalids[catename]) - len(sampleids[catename])
                #x = samplecounts[catename]
                #m = totalcounts[catename]
                #n = self.NumTotal - self.NumSamples - l
                #M = m + n
                #N = self.NumSamples
                a = samplecounts[catename]
                b = self.NumSamples - samplecounts[catename]
                c = len(totalids[catename]) - len(sampleids[catename])
                d = self.NumTotal - self.NumSamples - b
                pval = stats.chi2_contingency(np.array([[a, b],[c, d]]))
                #pval = stats.hypergeom.sf(x, M, m, N)
                self.Pvalues[catename] = pval[1]
    
        self.isTested = True
    
    def getTestResult(self, method = 'p-value', cutoff = 0.05):
        METHODS = ['p-value', 'q-value', 'none']
        
        if (not self.isTested):
            raise RuntimeError ("runTest is required\n")
        
        if (method not in METHODS):
            raise ValueError ("Method can only be %s" %(', '.join(METHODS)))
        
        if (method == 'q-value' and self.isAdjusted == False):
            raise RuntimeError ("p_adjust is required")
        
        Ids = []
        Desc = []
        TotalCounts = []
        ObservedCounts = []
        Expected = []
        Pvalues = []
        Qvalues = []
        
        for cid in self.Pvalues:
            if (method == 'p-value'):
                if (self.Pvalues[cid] > cutoff):
                    continue
            elif (method == 'q-value'):
                if (self.Qvalues[cid] > cutoff):
                    continue
            Ids.append(cid)
            Desc.append(self.TermDesc[cid])
            TotalCounts.append(self.TotalCounts[cid])
            ObservedCounts.append(self.SelectedCounts[cid])
            exp_val = self.TotalCounts[cid] / self.NumTotal * self.NumSamples
            Expected.append(exp_val)
            Pvalues.append(self.Pvalues[cid])
            if(self.isAdjusted):
                Qvalues.append(self.Qvalues[cid])
                
        if(self.isAdjusted):
            dataDict = {'ID':Ids, 'Description':Desc ,'Annotated':TotalCounts,
                    'Expected':Expected, 'Observed':ObservedCounts,
                    'p-value':Pvalues, 'q-value':Qvalues}
        else:
            dataDict = {'ID':Ids, 'Description':Desc ,'Annotated':TotalCounts,
                    'Expected':Expected, 'Observed':ObservedCounts,'p-value':Pvalues}
        
        df = pandas.DataFrame(dataDict)
        result = df.sort('p-value')
        return result
        
    def getSelCategoryGenes(self, category_name):
        if (category_name in self.SelectedIds):
            return self.SelectedIds[category_name]
        else:
            return None
    
    def getTotalCategroryGenes(self, category_name):
        if (category_name in self.TotalIds):
            return self.TotalIds[category_name]
        else:
            return None
    
    def p_adjust(self, correction_type = "BH"):
        if (not self.isTested):
            raise RuntimeError ("runTest is required first\n")
        
        pvalues = list(self.Pvalues.values())
        from numpy import array, empty
        pvalues = array(pvalues) 
        n = float(pvalues.shape[0]) 
        new_pvalues = empty(n)
        if correction_type == "Bonferroni":
            new_pvalues = n * pvalues
        elif correction_type == "Holm": #"Bonferroni-holm"
            values = [ (pvalue, i) for i, pvalue in enumerate(pvalues) ]
            values.sort()
            for rank, vals in enumerate(values):
                pvalue, i = vals
                new_pvalues[i] = (n-rank) * pvalue 
        elif correction_type == "BH": #"Benjamini-Hochberg
            values = [ (pvalue, i) for i, pvalue in enumerate(pvalues) ]
            values.sort()
            values.reverse()
            new_values = []
            for i, vals in enumerate(values): 
                rank = n - i
                pvalue, index = vals 
                new_values.append((n/rank) * pvalue)
            for i in range(0, int(n)-1):  
                if new_values[i] < new_values[i+1]:
                    new_values[i+1] = new_values[i]
            for i, vals in enumerate(values):
                pvalue, index = vals
                new_pvalues[index] = new_values[i]
        else:
            raise ValueError ("Correction types are 'Bonferroni', 'Holm' and 'BH'\n")
        
        keys = list(self.Pvalues.keys())
        
        for i in range(len(keys)):
            self.Qvalues[keys[i]] = new_pvalues[i]

        self.isAdjusted = True
    
    
    def _checkData(self):
        for gid in self.SelectedSamples:
            if(gid not in self.TotalSamples):
                raise ValueError ("%s is not in total samples" %(gid))
        
    def _countCategory(self, countList):
        catecounts = {}
        cateids = {}
        for gid in countList:
            if (gid not in self.GeneAnno):
                continue
            categories = self.GeneAnno[gid].split(';')
            for cid in categories:
                if (cid not in catecounts):
                    catecounts[cid] = 1
                    tmplist = []
                    tmplist.append(gid)
                    cateids[cid] = tmplist
                else:
                    catecounts[cid] += 1
                    cateids[cid].append(gid)
        return (catecounts, cateids)

def ReadInfoFile(filename):
    info = {}
    with open(filename, 'rU') as fh:
        reader = csv.reader(fh, delimiter = '\t')
        for line in reader:
            info[line[0]] = line[1]
    return info

def ReadGeneList(filename):
    gids = []
    with open(filename, 'rU') as fh:
        for line in fh:
            gids.append(line.strip())
    return gids

if __name__ == '__main__':
    if (len(sys.argv) < 5):
        sys.stderr.write("Usage:%s <GeneAnnoation> <TermDescription> <SelectedGenes> <TotalGenes> <OutFile>\n" %(sys.argv[0]))
        sys.exit(1)
    
    dictAnno = ReadInfoFile(sys.argv[1])
    dictDesc = ReadInfoFile(sys.argv[2])
    listSelGenes = ReadGeneList(sys.argv[3])
    listTotalGenes = ReadGeneList(sys.argv[4])
    
    hptest = EnrichmentTest(dictAnno, dictDesc, listSelGenes, listTotalGenes)
    hptest.runTest()
    hptest.p_adjust()
    res = hptest.getTestResult('q-value', 0.05)
    
    if(len(sys.argv) == 6):
        res.to_csv(sys.argv[5], columns = ['ID', 'Description', 'Annotated', 'Expected', 'Observed', 'p-value', 'q-value'])
    else:
        print(res)