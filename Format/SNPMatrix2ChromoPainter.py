#!/usr/bin/env python
import pandas
import csv
import sys
import copy

def snpfilter(row):
    values = row.values.tolist()
    if "." in values[4:]:
        return False
    else:
        if len(values[3]) > 1:
            return False
        return True
    
def postrans(row, chromlength):
    values = row.values.tolist()
    newpos = int(values[1]) + chromlength[values[0]]
    return newpos

def column2str(row):
    values = row.values.tolist()
    string = "".join(values)
    return string

def readPopInfo(popfile):
    fh_pop = open(popfile, 'rU')
    pops = {}
    popname = ''
    for line in fh_pop:
        lineinfo = line.strip()
        if lineinfo[0] == '@':
            popname = lineinfo[1:]
        else:
            pops[lineinfo] = popname
    fh_pop.close()
    return (pops)

def readChromLength(chromfile):
    chromlength = {}
    length = 0
    with open(chromfile, 'r') as fh_chrom:
        reader = csv.reader(fh_chrom, delimiter="\t")
        for line in reader:
            chromlength[line[0]] = length
            length += int(line[1])
    return chromlength

def getChromRegions(chromRow):
    values = chromRow.values.tolist()
    chrom = values[0]
    chromRange = {}
    chrRegion = {}
    chrRegion['s'] = 0
    for i in range(1,len(values)):
        if values[i] != chrom:
            chrRegion['e'] = i
            chromRange[chrom] = copy.copy(chrRegion)
            chrRegion['s'] = i
            chrom = values[i]
    chrRegion['e'] = len(values)
    chromRange[chrom] = copy.copy(chrRegion)
    return chromRange

if __name__ == '__main__':
    if len(sys.argv) < 4:
        sys.stderr.write ("Usage: <matrix_file> <pop_file> <chrom_length>\n")
        sys.exit(1)
    
    matrixfile = sys.argv[1]
    popfile = sys.argv[2]
    chromfile = sys.argv[3]
    
    table = pandas.read_table(matrixfile)
    tblFilter = table.apply(snpfilter, axis=1)
    table_flt = table[tblFilter]
    transTable = table_flt.transpose()
    
    pops = readPopInfo(popfile)
    chromlength = readChromLength(chromfile)
    #newpos = table_flt.apply(lambda x:postrans(x,chromlength), axis=1)
    #newpos = newpos.values.tolist()
    pos = transTable.iloc[1].values.tolist()
    
    genotypes = transTable[4:].apply(column2str,1)
    chromRegion = getChromRegions(transTable.iloc[0])
    
    for chrom in chromRegion:
        outHaplo = matrixfile + "." + chrom + ".haplo"
        tblDim = transTable.shape
        rgn = chromRegion[chrom]
        numSNP = rgn['e'] - rgn['s']
        with open(outHaplo, "w") as fh_outHaplo:
            fh_outHaplo.write("0 0\n%s\n%s\n" %(tblDim[0]-4, numSNP))
            fh_outHaplo.write("P")
            for i in range(rgn['s'],rgn['e']):
                fh_outHaplo.write(" %s" %(pos[i]))
            fh_outHaplo.write("\n")
            #print "number of samples: %s \n" %(len(genotypes))
            for i in range(len(genotypes)):
                fh_outHaplo.write(genotypes[i][rgn['s']:rgn['e']]+"\n")
    
    outLabel = matrixfile + ".label"
    strains = genotypes.index
    with open(outLabel, "w") as fh_outLabel:
        for i in range(len(strains)):
            fh_outLabel.write("%s %s 1\n" %(strains[i],pops[str(strains[i])]))
    