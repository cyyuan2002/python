#!/usr/bin/env python
from Bio import AlignIO
from Bio.Align import MultipleSeqAlignment as MultiAlign
import argparse
import sys
import csv
from Bio.Alphabet import IUPAC, Gapped

def readFile(FileName, FileType, TrimMode, TrimA, TrimB):
    Seqs = []
    fh_file = open (FileName,'rU')
    alignment = AlignIO.read(fh_file, FileType, alphabet=Gapped(IUPAC.ambiguous_dna))
    trimmed = MultiAlign(None,alphabet=Gapped(IUPAC.ambiguous_dna))
    fh_file.close()
    for seq in alignment:
        seqlen = len(seq)
        if TrimMode == 1:
            if(TrimA > 0):
                seqend = seqlen - TrimA
                seq = seq[TrimA:seqend]
        else:
            if TrimB == 0:
                seq = seq[(TrimA-1):]
            else:
                TrimB = TrimB - 1
                seq = seq[(TrimA-1):TrimB]
        trimmed.append(seq)
    return trimmed

def readGroupInfo(GroupFile):
    fhfile = open(GroupFile, 'rU')
    reader = csv.reader(fhfile, delimiter='\t')
    isogroup = {}
    for line in reader:
        isogroup[line[0]] = line[1]
    
    return (isogroup)

def generateSets(Seqs, Groupinfo):
    SeqGroups = {}
    for i in range(len(Seqs)):
        seqID = Seqs[i].id
        seqgroup = Groupinfo[seqID]
        tmplist = []
        if seqgroup in SeqGroups:
            tmplist = SeqGroups[seqgroup]
        tmplist.append(str(i+1))
        SeqGroups[seqgroup] = tmplist
    return SeqGroups

if __name__ == '__main__':

    INFORMATS = ["clustal","emboss","fasta","fasta-m10","ig","maf","nexus","phylip","phylip-sequential","phylip-relaxed","stockholm"]
    OUTFORMATS = ["clustal","fasta","maf","nexus","phylip","phylip-sequential","phylip-relaxed","stockholm"]
    
    parser = argparse.ArgumentParser(description='Convert alignment file among different formats:')
    parser.add_argument('-i', dest='inputfile', help='input alignment file')
    parser.add_argument('-f', dest='informat', help='format for the input file')
    parser.add_argument('-o', dest='outfile', help='output alignment file')
    parser.add_argument('-c', dest='outformat', help='format for the output file')
    parser.add_argument("-t", dest = 'trim_base', required = False,
                            type = int, help="bases trimmed on the both ends of aligned sequences")
    parser.add_argument("-s", dest = 'start_site', required = False, type = int,
                            help = "start site of the trimmed sequence")
    parser.add_argument("-e", dest = 'end_site', required = False, type = int,
                            help = "end site of the trimmed sequence")
    parser.add_argument("-g", dest = 'group_info', required = False, help = "group information for nexus file")
    
    args = parser.parse_args()
    
    if(len(sys.argv)==1):
        parser.print_help()
        sys.exit(1)
    
    infile = args.inputfile
    informat = args.informat
    outfile = args.outfile
    outformat = args.outformat
    TrimBase = args.trim_base
    TrimS = args.start_site
    TrimE = args.end_site
    groupfile = args.group_info
    
    if(informat not in INFORMATS):
        print(("%s format is not supported as input file\n" %(informat)))
        print(("supported input alignment formats are: "+", ".join(INFORMATS)))
        sys.exit(1)
    
    if(outformat not in OUTFORMATS):
        print(("%s format is not supported as output file\n" %(outformat)))
        print(("supported output alignment formats are: "+", ".join(OUTFORMATS)))
        sys.exit(1)
    
    TrimMode = 1
    if(TrimBase is None):
        if TrimS is None and TrimE is None:
            TrimMode = 1
            TrimBase = 0
        else:
            TrimMode = 2
            if TrimS is None:
                TrimS = 1
            if TrimE is None:
                TrimE = 0
            if(TrimE < TrimS):
                sys.stderr.write ("Error: End_site is smaller than Start_site\n")
                sys.exit(1)
    
    if TrimMode == 1:
        AlignSeq = readFile(infile, informat, 1, TrimBase, 0)
    else:
        AlignSeq = readFile(infile, informat, 2, TrimS, TrimE)

    fh_outfile = open(outfile,'w')
    AlignIO.write(AlignSeq,fh_outfile,outformat)
    
    if outformat == "nexus":
        if groupfile is not None:
            groupinfo = readGroupInfo(groupfile)
            seqsets = generateSets(AlignSeq,groupinfo)
            fh_outfile.write("BEGIN SETS;\n")
            clcall = []
            envall = []
            for setN in sorted(seqsets.keys()):
                fh_outfile.write("   TaxSet %s = %s;\n" %(setN," ".join(seqsets[setN])))
                if(setN[0] == 'C'):
                    clcall.extend(seqsets[setN])
                else:
                    envall.extend(seqsets[setN])
            fh_outfile.write("   TaxSet %s = %s;\n" %("clc"," ".join(clcall)))
            fh_outfile.write("   TaxSet %s = %s;\n" %("env"," ".join(envall)))
            fh_outfile.write("END;")
    
    fh_outfile.close()
    exit(0)
