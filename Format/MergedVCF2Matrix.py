#!/usr/bin/env python
"""This script is used to convert merged vcf to matrix"""
import sys
import re

if len(sys.argv) < 3:
    print(("usage:%s <merged_vcf> <outfile> <outputMode(1 snps, 2 all)>" %(sys.argv[0])))
    sys.exit(1)

inFile = sys.argv[1]
outFile = sys.argv[2]
if len(sys.argv) > 3:
    outputMode = int(sys.argv[3])
else:
    outputMode = 1


fh_in = open(inFile, 'rU')
fh_out = open(outFile, 'w')
for line in fh_in:
    if not re.search(r'^##', line):
        if re.search(r'^#', line):
            IDs = line.split('\t')
            fh_out.write ("CHROM\tPOS\tREF\tALT")
            for i in range(9, len(IDs)):
                name = re.findall(r'WGS.(\S+)', IDs[i])
                fh_out.write("\t%s" %(name[0]))
            fh_out.write("\n")
        else:
            stripped = line.strip()
            infos = stripped.split('\t')
            if(infos[4] == '.'):
                continue
            if(outputMode == 1):
                isskip = False
                if len(infos[3]) > 1:
                    isskip = True
                else:
                    alts = infos[4].split(',')
                    for i in range(len(alts)):
                        if(len(alts[i]) > 1):
                            isskip = True
                            break
                if not isskip:
                    fh_out.write("%s\t%s\t%s\t%s" %(infos[0], infos[1], \
                                    infos[3], infos[4]))
                    for i in range(9, len(IDs)):
                        fh_out.write("\t%s" %(infos[i][0]))
                    fh_out.write("\n")
            else:
                fh_out.write("%s\t%s\t%s\t%s" %(infos[0], infos[1], \
                                    infos[3], infos[4]))
                for i in range(9, len(IDs)):
                    fh_out.write("\t%s" %(infos[i][0]))
                fh_out.write("\n")
fh_in.close()
