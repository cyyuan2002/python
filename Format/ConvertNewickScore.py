#!/usr/bin/env python
import sys

File = sys.argv[1]
fh_file = open(File,'r')
treetxt = next(fh_file)
branketpos = 0
isbranket = 0
stringcur = 0
newtreestr = ""

for i in range(0,len(treetxt)):
    if(treetxt[i] == ')'):
        if(isbranket == 1):
            confidence = treetxt[branketpos:i]
            confint = int(float(confidence)*100)
            newtreestr += str(confint) + ")"
            branketpos = i+2
        else:
            branketpos = i+1
            isbranket = 1
            newtreestr += treetxt[stringcur:branketpos]
    elif(treetxt[i] == ',' and isbranket == 1):
        confidence = treetxt[branketpos:i]
        confint = int(float(confidence)*100)
        newtreestr += str(confint)
        stringcur = i
        isbranket = 0
newtreestr += ';'
print (newtreestr)
sys.exit(0)
