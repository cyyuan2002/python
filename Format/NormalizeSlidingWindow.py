#!/usr/bin/env python

import sys
import pandas
from math import sqrt

if len(sys.argv) < 3:
    sys.stderr.write("Usage:%s <Coverage_depth_file> <outfile>" %(sys.argv[0]))
    sys.exit(1)

infile = sys.argv[1]
outfile = sys.argv[2]
df = pandas.read_csv(infile, sep="\t", header = None)

cover = sorted(df[3])
medium = cover[int(len(cover)/2)]
df[3] = [round(x,2) for x in df[3]/medium]
df.to_csv(outfile, sep="\t", header = False, index = False)
