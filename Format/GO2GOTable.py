#!/usr/bin/env python
import csv
import sys

filename = sys.argv[1]

geneGO = {}
with open(filename,'r') as csv_GO:
    reader = csv.reader(csv_GO,delimiter='\t')
    for line in reader:
        geneInfo = line[0].split(' ')
        geneID = geneInfo[2]
        gID = geneID.replace("locus=","")
        GOID = line[1]
        if(gID in geneGO):
            geneGO[gID] += ', '+GOID
        else:
            geneGO[gID] = GOID

for gene in geneGO:
    print((gene+"\t"+geneGO[gene]))