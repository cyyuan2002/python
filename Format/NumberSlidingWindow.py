#!/usr/bin/env python
"""This script is used to create sliding window based on the givin genome for
making figures in R or other software.

Sample of input data:
supercont2.1_of_Cryptococcus_neoformans_grubii_H99	959002	4
supercont2.1_of_Cryptococcus_neoformans_grubii_H99	959011	4
supercont2.1_of_Cryptococcus_neoformans_grubii_H99	959015	4
supercont2.1_of_Cryptococcus_neoformans_grubii_H99	959018	3
supercont2.1_of_Cryptococcus_neoformans_grubii_H99	968361	3
supercont2.1_of_Cryptococcus_neoformans_grubii_H99	969906	3
supercont2.1_of_Cryptococcus_neoformans_grubii_H99	969980	3
supercont2.1_of_Cryptococcus_neoformans_grubii_H99	970059	3
supercont2.1_of_Cryptococcus_neoformans_grubii_H99	970620	3
supercont2.1_of_Cryptococcus_neoformans_grubii_H99	971222	4

Sample of genome size file:
supercont2.1_of_Cryptococcus_neoformans_grubii_H99  2291499
supercont2.2_of_Cryptococcus_neoformans_grubii_H99  1621675
supercont2.3_of_Cryptococcus_neoformans_grubii_H99  1575141
supercont2.4_of_Cryptococcus_neoformans_grubii_H99  1084805
supercont2.5_of_Cryptococcus_neoformans_grubii_H99  1814975
supercont2.6_of_Cryptococcus_neoformans_grubii_H99  1422463
"""
import sys
import csv
import copy
from math import ceil

if len(sys.argv) < 5:
    sys.stderr.write ("usage:%s <input_file> <genome_info> <windowsize> <stepsize>\n" %(sys.argv[0]))
    sys.exit(1)
    
input_file = sys.argv[1]
genome_info = sys.argv[2]
window_size = int(sys.argv[3])
step_size = int(sys.argv[4])

chrominfo = {}
chromorder = []
windowinfo = {}

gfile_fh = open(genome_info, 'rU')
gfile_reader = csv.reader(gfile_fh, delimiter = "\t")

for line in gfile_reader:
    chromlength = int(line[1])
    chrominfo[line[0]] = chromlength
    chromorder.append(line[0])
    tempdata = []
    #init 0 for each window
    for i in range(int((chromlength - window_size)/step_size)+1):
        tempdata.append(0)
    windowinfo[line[0]] = tempdata[:]
gfile_fh.close()

ifile_fh = open(input_file, 'rU')
ifile_reader = csv.reader(ifile_fh, delimiter = "\t")
lastchrom = ""

for line in ifile_reader:
    if lastchrom != line[0]:
        if lastchrom != "":
            windowinfo[lastchrom] = list(chromdata[:])
        chromdata = list(windowinfo[line[0]][:])
        lastchrom = line[0]
    numwindows = len(chromdata)
    if(int(line[1]) > numwindows * step_size):
        continue
    startwindow = int(ceil((int(line[1]) - window_size )/step_size))
    if(startwindow < 0):
        startwindow = 0
    endwindow = int(ceil(int(line[1])/step_size))
    if(endwindow > numwindows):
        endwindow = numwindows
    for i in range(startwindow,endwindow):
        chromdata[i] += int(line[2])

windowinfo[line[0]] = list(chromdata[:])
ifile_fh.close()

chromid = 1
for chrom in chromorder:
    chromdata = windowinfo[chrom]
    for i in range(len(chromdata)):
        start = i*step_size + 1
        end = start + window_size - 1
        site = str(start) + '-' + str(end)
        print(("%s\t%s\t%s\t%s" %(chrom, chromid,site, chromdata[i])))
    chromid += 1
