#!/usr/bin/env python
import sys
import glob
import os
import re

if len(sys.argv) < 3:
    sys.stderr.write("Usage:%s <input_folder> <output_folder> <ext>\n" %sys.argv[0])
    sys.exit(1)

in_folder = sys.argv[1]
out_folder = sys.argv[2]
if sys.argv[3] is None:
    ext = "fastq.gz"
else:
    ext = sys.argv[3]

if not os.path.isdir(in_folder):
    raise IOError("Cannot find folder %s" %in_folder)

if not os.path.isdir(out_folder):
    os.mkdir(out_folder)

source_files = sorted(glob.glob("%s/*.%s" % (in_folder, ext)))
for file in source_files:
    fn = os.path.basename(file)
    m = re.match(r'^(\S+)-(DNA|RNA)\S+_R(1|2)\S+', fn)
    out_file = m.group(1) + "-" + m.group(2) + "_" + m.group(3) + ".fastq"
    out_file_full = out_folder + os.sep + out_file
    sys.stdout.write("Decompress %s to %s\n" % (file, out_file_full))
    os.system("gzip -dc %s >> %s" % (file, out_file_full))

sys.stdout.write("Job finished!")
sys.exit(0)

