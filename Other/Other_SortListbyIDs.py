#!/usr/bin/env python

import sys
import csv

def SortInfo (IDs, Info):
    for ID in IDs:
        if(ID in Info):
            print((Info[ID]))

if __name__ == '__main__':
    if len(sys.argv) < 5:
        sys.stderr.write("Usage:%s <ID_File> <ID_Field> <Info_File> <Info_Field>")
        sys.exit(1)
    IDfile = sys.argv[1]
    ID_field = int(sys.argv[2])
    Infofile = sys.argv[3]
    Info_field = int(sys.argv[4])
    
    IDs = []
    fh_idfile = open(IDfile, 'rU')
    id_reader = csv.reader(fh_idfile, delimiter="\t")
    for line in id_reader:
        IDs.append(line[ID_field])
    fh_idfile.close()
    
    fh_infofile = open(Infofile, 'rU')
    info_reader = csv.reader(fh_infofile, delimiter="\t")
    infos = {}
    for line in info_reader:
        infos[line[Info_field]] = '\t'.join(line)
    fh_infofile.close()
    
    SortInfo(IDs, infos)
    